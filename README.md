#SocialNetBalch

**Functionality:**

+ registration  
+ authentication  
+ ajax search with pagination  
+ display profile  
+ edit profile  
+ upload avatar  
+ users export to xml  
+ groups
+ messages

**Tools:**  
JDK 8, SpringBoot 2, Spring Web MVC, Spring Security, Spring Data, JMS 2, JPA 2 / Hibernate 5, FasterXML 2, jQuery 3, Twitter Bootstrap 4, JUnit 4, Mockito 2, Maven 3, Git / Bitbucket, Tomcat 8, MySQL/H2, Heroku Cloud, IntelliJIDEA 2018.

**Heroku:**  
http://socialnetbalch.herokuapp.com  
login/password:  
koltigal@gmail.com/password  
Pochuichik@gmail.com/password  

**Screenshots:**  
https://gyazo.com/14b6d73b4b2b54e421bd7be23aee25c9  
https://gyazo.com/1fb806e7c93fd87d1bf09a2d24c4d246  
https://gyazo.com/fb1188692f0c41115ff46205a1602d06  
https://gyazo.com/acf578e61cd95f6bdc0f8ef7241c1094

--  
**����������� �������**  
������� getJavaJob,   
[http://www.getjavajob.com](http://www.getjavajob.com)
