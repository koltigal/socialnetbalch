package com.getjavajob.training.webproject1803.balchevskiyn;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "friend_requests")
public class FriendRequest {

    @EmbeddedId
    protected FriendRequestPK friendRequestPK;
    @OneToOne
    @JoinColumn(name = "requesting_user_id", insertable = false, updatable = false)
    private Account requestingUser;
    @OneToOne
    @JoinColumn(name = "requested_user_id", insertable = false, updatable = false)
    private Account requestedUser;

    public FriendRequest() {
    }

    public FriendRequest(Account requestingUser, Account requestedUser) {
        this.requestingUser = requestingUser;
        this.requestedUser = requestedUser;
        this.friendRequestPK = new FriendRequestPK(requestingUser.getId(), requestedUser.getId());
    }

    public Account getRequestingUser() {
        return requestingUser;
    }

    public void setRequestingUser(Account requestingUser) {
        this.requestingUser = requestingUser;
    }

    public Account getRequestedUser() {
        return requestedUser;
    }

    public void setRequestedUser(Account requestedUser) {
        this.requestedUser = requestedUser;
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestingUser, requestedUser);
    }

    @Embeddable
    static class FriendRequestPK implements Serializable {

        @Column(name = "requesting_user_id")
        private Integer requestingUser;
        @Column(name = "requested_user_id")
        private Integer requestedUser;

        public FriendRequestPK() {
        }

        public FriendRequestPK(Integer requestingUser, Integer requestedUser) {
            this.requestingUser = requestingUser;
            this.requestedUser = requestedUser;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            FriendRequestPK that = (FriendRequestPK) o;
            return Objects.equals(requestingUser, that.requestingUser) &&
                    Objects.equals(requestedUser, that.requestedUser);
        }

        @Override
        public int hashCode() {
            return Objects.hash(requestingUser, requestedUser);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FriendRequest that = (FriendRequest) o;
        return Objects.equals(requestingUser, that.requestingUser)
                && Objects.equals(requestedUser, that.requestedUser);
    }

}
