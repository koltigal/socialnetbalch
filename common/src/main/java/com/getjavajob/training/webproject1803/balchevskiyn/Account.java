package com.getjavajob.training.webproject1803.balchevskiyn;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Formula;

@SuppressWarnings("unused")
@XmlRootElement
@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @XmlElement(nillable = true, name = "id")
    private Integer id;
    @Column(name = "full_name")
    private String name;
    @Column(name = "birth")
    private Date birthDate;
    @ElementCollection
    @CollectionTable(name = "phones", joinColumns = @JoinColumn(name = "user_id"))
    @Column(name = "phone")
    private List<String> phones;
    private String email;
    private Integer icq;
    private String skype;
    private String password;
    @Enumerated(EnumType.STRING)
    private AccountRole role;
    @Formula("avatar is not null")
    private boolean avatarExists;
    @Lob
    private byte[] avatar;
    @Column(name = "create_date")
    private Date createDate;
    @ElementCollection
    @CollectionTable(name = "friends", joinColumns = @JoinColumn(name = "user_id"))
    @Column(name = "user_friend_id")
    private List<Integer> friendsIdList;

    public Account() {
    }

    public Account(String name, Date birthDate, List<String> phones, String email, Integer icq, String skype,
            String password, AccountRole role, boolean avatarExists, byte[] avatar, Date createDate) {
        this.name = name;
        this.birthDate = birthDate;
        this.phones = phones;
        this.email = email;
        this.icq = icq;
        this.skype = skype;
        this.password = password;
        this.role = role;
        this.avatarExists = avatarExists;
        this.avatar = avatar;
        this.createDate = createDate;
    }

    public Account(int id, String name, Date birthDate, List<String> phones, String email, Integer icq, String skype,
            String password, AccountRole role, boolean avatarExists, byte[] avatar, Date createDate,
            List<Integer> friendsIdList) {
        this(name, birthDate, phones, email, icq, skype, password, role, avatarExists, avatar, createDate);
        this.id = id;
        this.friendsIdList = friendsIdList;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    @XmlElement
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getStringBirthDate() {
        if (birthDate == null) {
            return "";
        } else {
            return (new SimpleDateFormat("yyyy-MM-dd")).format(birthDate);
        }
    }

    public List<String> getPhones() {
        return phones;
    }

    @XmlElement
    @XmlElementWrapper
    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public String getEmail() {
        return email;
    }

    @XmlElement
    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIcq() {
        return icq;
    }

    @XmlElement
    public void setIcq(Integer icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    @XmlElement
    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getPassword() {
        return password;
    }

    @XmlTransient
    public void setPassword(String password) {
        this.password = password;
    }

    public AccountRole getRole() {
        return role;
    }

    @XmlElement
    public void setRole(AccountRole role) {
        this.role = role;
    }

    public boolean isAvatarExists() {
        return avatarExists;
    }

    @XmlElement
    public void setAvatarExists(boolean avatarExists) {
        this.avatarExists = avatarExists;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    @XmlTransient
    public void setAvatar(byte[] avatar) {
        this.avatar = avatar;
    }

    public Date getCreateDate() {
        return createDate;
    }

    @XmlElement
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getStringCreateDate() {
        return createDate == null ? "" : new SimpleDateFormat("yyyy-MM-dd").format(createDate);
    }

    public String getStringRole() {
        return role.toString();
    }

    public List<Integer> getFriendsIdList() {
        return friendsIdList;
    }

    @XmlElement
    @XmlElementWrapper
    public void setFriendsIdList(List<Integer> friendsIdList) {
        this.friendsIdList = friendsIdList;
    }

    @Override
    public int hashCode() {
        int result = Objects
                .hash(
                        id, name, birthDate, phones, email, icq, skype, password, role, avatarExists, createDate,
                        friendsIdList);
        result = 31 * result + Arrays.hashCode(avatar);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return avatarExists == account.avatarExists &&
                id.equals(account.id) &&
                name.equals(account.name) &&
                Objects.equals(birthDate, account.birthDate) &&
                Objects.equals(phones, account.phones) &&
                email.equals(account.email) &&
                Objects.equals(icq, account.icq) &&
                Objects.equals(skype, account.skype) &&
                password.equals(account.password) &&
                role == account.role &&
                Arrays.equals(avatar, account.avatar) &&
                createDate.equals(account.createDate) &&
                Objects.equals(friendsIdList, account.friendsIdList);
    }

    public enum AccountRole {
        USER, ADMIN
    }
}
