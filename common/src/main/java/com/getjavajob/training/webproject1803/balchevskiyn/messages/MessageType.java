package com.getjavajob.training.webproject1803.balchevskiyn.messages;

public enum MessageType {
    PERSONAL, WALL, COMMUNITY
}

