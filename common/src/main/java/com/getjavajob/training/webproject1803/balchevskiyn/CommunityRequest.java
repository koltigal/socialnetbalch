package com.getjavajob.training.webproject1803.balchevskiyn;

import com.getjavajob.training.webproject1803.balchevskiyn.CommunityRequest.CommunityRequestPK;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@SuppressWarnings("unused")
@Entity
@IdClass(CommunityRequestPK.class)
@Table(name = "community_requests")
public class CommunityRequest {

    @Id
    @OneToOne
    @JoinColumn(name = "requesting_user_id")
    private Account requestingUser;
    @Id
    @OneToOne
    @JoinColumn(name = "requested_community_id")
    private Community requestedCommunity;
    private String message;

    public CommunityRequest() {
    }

    public CommunityRequest(Account requestingUser, Community requestedCommunity, String message) {
        this.requestingUser = requestingUser;
        this.requestedCommunity = requestedCommunity;
        this.message = message;
    }

    public Account getRequestingUser() {
        return requestingUser;
    }

    public void setRequestingUser(Account requestingUser) {
        this.requestingUser = requestingUser;
    }

    public Community getRequestedCommunity() {
        return requestedCommunity;
    }

    public void setRequestedCommunity(Community requestedCommunity) {
        this.requestedCommunity = requestedCommunity;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestingUser, requestedCommunity, message);
    }

    static class CommunityRequestPK implements Serializable {

        private Account requestingUser;
        private Community requestedCommunity;

        public CommunityRequestPK(
                Account requestingUser, Community requestedCommunity) {
            this.requestingUser = requestingUser;
            this.requestedCommunity = requestedCommunity;
        }

        public CommunityRequestPK() {

        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            CommunityRequestPK that = (CommunityRequestPK) o;
            return requestingUser.equals(that.requestingUser) &&
                    requestedCommunity.equals(that.requestedCommunity);
        }

        @Override
        public int hashCode() {
            return Objects.hash(requestingUser, requestedCommunity);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommunityRequest that = (CommunityRequest) o;
        return Objects.equals(requestingUser, that.requestingUser)
                && Objects.equals(requestedCommunity, that.requestedCommunity)
                && Objects.equals(message, that.message);
    }

}

