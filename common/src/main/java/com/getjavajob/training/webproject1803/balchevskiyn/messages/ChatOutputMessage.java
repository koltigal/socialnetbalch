package com.getjavajob.training.webproject1803.balchevskiyn.messages;

public class ChatOutputMessage extends ChatMessage {

    private String time;

    public ChatOutputMessage(final Integer fromId, final String fromName, final String fromEmail, final String text,
            final String time) {
        super(fromId, fromName, fromEmail, null, text);
        this.time = time;
    }

    public String getTime() {
        return time;
    }

}
