package com.getjavajob.training.webproject1803.balchevskiyn.mailing;

import java.io.Serializable;
import java.util.List;

public class BirthdayData implements Serializable {

    public final String birthdayAccountName;
    public final List<String> accountFriendsEmails;

    public BirthdayData(List<String> accountFriendsEmails, String birthdayAccountName) {
        this.accountFriendsEmails = accountFriendsEmails;
        this.birthdayAccountName = birthdayAccountName;
    }

    @Override
    public String toString() {
        String stringResult = "BirthdayAccountName = " + birthdayAccountName + "; Emails =";
        for (String friendEmail : accountFriendsEmails) {
            stringResult += " " + friendEmail;
        }
        return stringResult;
    }
}
