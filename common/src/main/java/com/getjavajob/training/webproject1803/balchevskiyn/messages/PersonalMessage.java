package com.getjavajob.training.webproject1803.balchevskiyn.messages;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "personal_messages")
public class PersonalMessage extends RootMessage<Account> {

    public PersonalMessage() {
    }

    public PersonalMessage(Integer id, Account sentFrom, Account sentTo, String text, LocalDate date) {
        super(id, sentFrom, sentTo, text, false, null, date);
    }

    @Override
    public Account getReceiver() {
        return receiver;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), receiver);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        PersonalMessage that = (PersonalMessage) o;
        return Objects.equals(receiver, that.receiver);
    }
}
