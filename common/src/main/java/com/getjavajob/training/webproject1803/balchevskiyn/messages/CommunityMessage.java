package com.getjavajob.training.webproject1803.balchevskiyn.messages;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.Community;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "community_messages")
public class CommunityMessage extends RootMessage<Community> {

    public CommunityMessage() {
    }

    public CommunityMessage(
            Integer id, Account sentFrom, Community sentTo, String text, boolean imageExists, byte[] photo,
            LocalDate date) {
        super(id, sentFrom, sentTo, text, imageExists, photo, date);
    }

    @Override
    public Community getReceiver() {
        return receiver;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), receiver);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        CommunityMessage that = (CommunityMessage) o;
        return Objects.equals(receiver, that.receiver);
    }
}
