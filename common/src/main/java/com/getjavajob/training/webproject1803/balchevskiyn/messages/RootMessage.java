package com.getjavajob.training.webproject1803.balchevskiyn.messages;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Formula;

@SuppressWarnings("unused")
@MappedSuperclass
public class RootMessage<T> {

    @OneToOne
    @JoinColumn(name = "receiver")
    protected T receiver;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne
    @JoinColumn(name = "sender")
    private Account sender;
    private String text;
    @Formula("photo is not null")
    private boolean photoExists;
    private byte[] photo;
    private LocalDate date;

    public RootMessage() {
    }

    public RootMessage(Integer id, Account sentFrom, T sentTo, String text, boolean photoExists, byte[] photo,
            LocalDate date) {
        this.id = id;
        this.sender = sentFrom;
        this.receiver = sentTo;
        this.text = text;
        this.photoExists = photoExists;
        this.photo = photo;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Account getSender() {
        return sender;
    }

    public void setSender(Account sender) {
        this.sender = sender;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Object getReceiver() {
        return receiver;
    }

    public void setReceiver(T receiver) {
        this.receiver = receiver;
    }

    @SuppressWarnings("unused")
    public boolean isPhotoExists() {
        return photoExists;
    }

    public void setPhotoExists(boolean photoExists) {
        this.photoExists = photoExists;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, receiver, sender, text, photoExists, date);
        result = 31 * result + Arrays.hashCode(photo);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RootMessage<?> that = (RootMessage<?>) o;
        return photoExists == that.photoExists &&
                Objects.equals(id, that.id) &&
                receiver.equals(that.receiver) &&
                sender.equals(that.sender) &&
                Objects.equals(text, that.text) &&
                Arrays.equals(photo, that.photo) &&
                Objects.equals(date, that.date);
    }
}
