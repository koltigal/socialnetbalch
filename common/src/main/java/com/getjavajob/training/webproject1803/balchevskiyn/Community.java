package com.getjavajob.training.webproject1803.balchevskiyn;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Formula;

@SuppressWarnings("unused")
@Entity
@Table(name = "communities")
public class Community {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "community_name")
    private String communityName;
    @Formula("community_avatar is not null")
    private boolean avatarExists;
    @Lob
    @Column(name = "community_avatar")
    private byte[] communityAvatar;
    @Column(name = "create_date")
    private LocalDate createDate;
    private String description;
    @Column(name = "creator_id")
    private Integer creatorId;
    @JsonManagedReference
    @OneToMany(mappedBy = "community", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<CommunityUser> users;

    public Community() {
    }

    public Community(String communityName, boolean avatarExists, LocalDate createDate,
            String description, Integer creatorId) {
        this.communityName = communityName;
        this.avatarExists = avatarExists;
        this.createDate = createDate;
        this.description = description;
        this.creatorId = creatorId;
        this.users = new ArrayList<>();
    }

    public Community(int id, String communityName, boolean avatarExists, LocalDate createDate,
            String description, Integer creatorId) {
        this(communityName, avatarExists, createDate, description, creatorId);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public boolean isAvatarExists() {
        return avatarExists;
    }

    public void setAvatarExists(boolean avatarExists) {
        this.avatarExists = avatarExists;
    }

    public List<CommunityUser> getUsers() {
        return users;
    }

    public void setUsers(List<CommunityUser> users) {
        this.users = users;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getCommunityAvatar() {
        return communityAvatar;
    }

    public void setCommunityAvatar(byte[] communityAvatar) {
        this.communityAvatar = communityAvatar;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, communityName, avatarExists, createDate, description, creatorId);
        result = 31 * result + Arrays.hashCode(communityAvatar);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Community community = (Community) o;
        return avatarExists == community.avatarExists &&
                id.equals(community.id) &&
                communityName.equals(community.communityName) &&
                Arrays.equals(communityAvatar, community.communityAvatar) &&
                createDate.equals(community.createDate) &&
                Objects.equals(description, community.description) &&
                creatorId.equals(community.creatorId);
    }
}
