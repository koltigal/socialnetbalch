package com.getjavajob.training.webproject1803.balchevskiyn.messages;

public class ChatMessage {

    private Integer fromId;
    private String fromName;
    private String fromEmail;
    private String toEmail;
    private String text;

    public ChatMessage() {
    }

    public ChatMessage(Integer fromId, String fromName, String fromEmail, String toEmail, String text) {
        this.fromId = fromId;
        this.fromName = fromName;
        this.fromEmail = fromEmail;
        this.toEmail = toEmail;
        this.text = text;
    }

    public Integer getFromId() {
        return fromId;
    }

    public String getFromName() {
        return fromName;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public String getToEmail() {
        return toEmail;
    }

    public String getText() {
        return text;
    }
}
