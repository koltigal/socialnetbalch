package com.getjavajob.training.webproject1803.balchevskiyn.messages;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "wall_messages")
public class WallMessage extends RootMessage<Account> {

    public WallMessage() {
    }

    public WallMessage(Integer id, Account sentFrom, Account sentTo, String text, boolean imageExists, byte[] photo,
            LocalDate date) {
        super(id, sentFrom, sentTo, text, imageExists, photo, date);
    }

    @Override
    public Account getReceiver() {
        return receiver;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), receiver);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        WallMessage that = (WallMessage) o;
        return Objects.equals(receiver, that.receiver);
    }
}
