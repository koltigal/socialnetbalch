package com.getjavajob.training.webproject1803.balchevskiyn;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.getjavajob.training.webproject1803.balchevskiyn.CommunityUser.CommunityUserPK;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "community_users")
@IdClass(CommunityUserPK.class)
public class CommunityUser {

    @Id
    @OneToOne
    @JoinColumn(name = "user_id")
    private Account account;
    @Id
    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "community_id", nullable = false)
    private Community community;
    @Enumerated(EnumType.STRING)
    @Column(name = "user_role")
    private CommunityUserRole communityUserRole;

    public CommunityUser() {
    }

    public CommunityUser(
            Account account, Community community,
            CommunityUserRole communityUserRole) {
        this.account = account;
        this.community = community;
        this.communityUserRole = communityUserRole;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Community getCommunity() {
        return community;
    }

    public void setCommunity(Community community) {
        this.community = community;
    }

    public CommunityUserRole getCommunityUserRole() {
        return communityUserRole;
    }

    public void setCommunityUserRole(
            CommunityUserRole communityUserRole) {
        this.communityUserRole = communityUserRole;
    }

    @Override
    public int hashCode() {
        return Objects.hash(account, community, communityUserRole);
    }

    public enum CommunityUserRole {
        USER, ADMIN
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommunityUser that = (CommunityUser) o;
        return account.equals(that.account) &&
                community.equals(that.community) &&
                communityUserRole == that.communityUserRole;
    }

    static class CommunityUserPK implements Serializable {

        private Account account;
        private Community community;

        public CommunityUserPK() {
        }

        public CommunityUserPK(Account user, Community community) {
            this.account = user;
            this.community = community;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            CommunityUserPK that = (CommunityUserPK) o;
            return account.equals(that.account) &&
                    community.equals(that.community);
        }

        @Override
        public int hashCode() {
            return Objects.hash(account, community);
        }
    }
}


