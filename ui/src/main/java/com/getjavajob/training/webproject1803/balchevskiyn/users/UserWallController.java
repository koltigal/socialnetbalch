package com.getjavajob.training.webproject1803.balchevskiyn.users;

import com.getjavajob.training.webproject1803.balchevskiyn.messages.RootMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.WallMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.service.MessageService;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/user-wall")
public class UserWallController {

    private static final Logger logger = LoggerFactory.getLogger(UserWallController.class);
    @Autowired
    private MessageService messageService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showUserWall(@SessionAttribute("receiverId") int receiverId) {
        List<RootMessage> wallChat = messageService.getMessagesByReceiverAndType(receiverId, WallMessage.class);
        ModelAndView modelAndView = new ModelAndView("users/userWallPage");
        modelAndView.addObject("wallChat", wallChat);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String postMessageOnUserWall(
            @SessionAttribute("receiverId") int receiverId,
            @SessionAttribute("userId") int userId, @RequestParam("wallMessageText") String wallMessageText,
            @RequestParam("photo") MultipartFile photo) throws IOException {
        if (!wallMessageText.isEmpty()) {
            int newMessageId = messageService.addMessageBySenderReceiverAndType(userId, receiverId, wallMessageText,
                    WallMessage.class);
            if (photo.getSize() > 0) {
                messageService.insertMessageImageById(newMessageId, photo.getBytes(), WallMessage.class);
            }
        } else if (photo.getSize() > 0) {
            int newMessageId = messageService.addMessageBySenderReceiverAndType(userId, receiverId, wallMessageText,
                    WallMessage.class);
            messageService.insertMessageImageById(newMessageId, photo.getBytes(), WallMessage.class);
        }
        return "redirect:user-page?pageUserId=" + receiverId;
    }
}
