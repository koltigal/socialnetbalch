package com.getjavajob.training.webproject1803.balchevskiyn.security;

import javax.servlet.http.HttpServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/no-access")
public class NoAccessController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(NoAccessController.class);

    @RequestMapping()
    public ModelAndView showPage() {
        ModelAndView modelAndView = new ModelAndView("security/noAccessPage");
        return modelAndView;
    }
}
