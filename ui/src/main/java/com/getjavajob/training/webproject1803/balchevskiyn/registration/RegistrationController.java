package com.getjavajob.training.webproject1803.balchevskiyn.registration;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.Util.printMessageAndRedirect;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.model.AccountUiWrapper;
import com.getjavajob.training.webproject1803.balchevskiyn.service.AccountService;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/register")
public class RegistrationController {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);
    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showRegisterPage() {
        return new ModelAndView("registration/registrationPage");
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView register(
            @ModelAttribute AccountUiWrapper accountUiWrapper,
            @RequestParam("avatar") MultipartFile avatar) throws IOException {
        Account newAccount = accountUiWrapper.getAccount();
        accountService.createAccount(newAccount, avatar.getSize() > 0 ? avatar.getBytes() : null);
        return printMessageAndRedirect("authentication", "Вы успешно зарегистрировались!");
    }
}
