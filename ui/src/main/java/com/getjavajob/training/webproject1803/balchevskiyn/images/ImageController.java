package com.getjavajob.training.webproject1803.balchevskiyn.images;

import com.getjavajob.training.webproject1803.balchevskiyn.messages.CommunityMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.PersonalMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.WallMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.service.AccountService;
import com.getjavajob.training.webproject1803.balchevskiyn.service.CommunityService;
import com.getjavajob.training.webproject1803.balchevskiyn.service.MessageService;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URLConnection;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/images")
public class ImageController {

    private static final Logger logger = LoggerFactory.getLogger(ImageController.class);
    @Autowired
    private CommunityService communityService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.GET)
    public void showImage(
            @RequestParam("imageType") String imageType, @RequestParam("imageId") int imageId,
            HttpServletResponse resp)
            throws IOException {
        byte[] imageBytes;
        switch (imageType) {
            case "personAvatar":
                imageBytes = accountService.getAvatarById(imageId);
                break;
            case "groupAvatar":
                imageBytes = communityService.getCommunityAvatarById(imageId);
                break;
            case "personalMessageImage":
                imageBytes = messageService.getMessageImageById(imageId, PersonalMessage.class);
                break;
            case "wallMessageImage":
                imageBytes = messageService.getMessageImageById(imageId, WallMessage.class);
                break;
            case "communityMessageImage":
                imageBytes = messageService.getMessageImageById(imageId, CommunityMessage.class);
                break;
            default:
                throw new IllegalArgumentException();
        }
        if (imageBytes != null) {
            String contentType = URLConnection.guessContentTypeFromStream(new ByteArrayInputStream(imageBytes));
            resp.setContentType(contentType);
            resp.setContentLength(imageBytes.length);
            resp.getOutputStream().write(imageBytes);
        }
    }
}
