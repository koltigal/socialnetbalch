package com.getjavajob.training.webproject1803.balchevskiyn.communities;

import com.getjavajob.training.webproject1803.balchevskiyn.CommunityRequest;
import com.getjavajob.training.webproject1803.balchevskiyn.service.CommunityRequestService;
import com.getjavajob.training.webproject1803.balchevskiyn.service.CommunityService;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/community-requests")
public class CommunityRequestsController {

    private static final Logger logger = LoggerFactory.getLogger(CommunityRequestsController.class);
    @Autowired
    private CommunityService communityService;
    @Autowired
    private CommunityRequestService communityRequestService;

    @RequestMapping(method = RequestMethod.GET)
    protected ModelAndView showCommunityRequests(@SessionAttribute("userId") int userId) {
        List<CommunityRequest> communityRequestsToAccount = new ArrayList<>();
        for (Integer anAdminCommunitiesId : communityService.getIdOfCommunitiesWhereUserIsAdmin(userId)) {
            communityRequestsToAccount.addAll(communityRequestService.getRequestsToCommunity(anAdminCommunitiesId));
        }
        ModelAndView modelAndView = new ModelAndView("communities/communityRequestsPage");
        modelAndView.addObject("communityRequestsToAccount", communityRequestsToAccount);
        modelAndView.addObject(
                "communityRequestsFromAccount",
                communityRequestService.getCommunityRequestsFromAccount(userId));
        return modelAndView;
    }
}
