package com.getjavajob.training.webproject1803.balchevskiyn.model;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.Account.AccountRole;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class AccountUiWrapper {

    private static final Logger logger = LoggerFactory.getLogger(AccountUiWrapper.class);

    private String uiName;
    private String uiBirthDate;
    private List<String> uiPhones;
    private String uiEmail;
    private String uiIcq;
    private String uiSkype;
    private String uiPassword;

    public AccountUiWrapper(
            String name, String birthDate, String phone, String email, String icq, String skype,
            String password) {
        this.uiName = name;
        this.uiBirthDate = birthDate;
        this.uiPhones = new ArrayList<>();
        uiPhones.add(phone);
        this.uiEmail = email;
        this.uiIcq = icq;
        this.uiSkype = skype;
        this.uiPassword = password;
    }

    public Account getAccount() {
        String password = (new BCryptPasswordEncoder(11)).encode(uiPassword);
        Date birthDate = uiBirthDate.isEmpty() ? null : Date.valueOf(uiBirthDate);
        String skype = uiSkype.isEmpty() ? null : uiSkype;
        Integer icq = uiIcq.isEmpty() ? null : Integer.valueOf(uiIcq);
        return new Account(uiName, birthDate, uiPhones, uiEmail, icq, skype, password,
                AccountRole.valueOf("USER"), false, null, Date.valueOf(LocalDate.now()));
    }
}
