package com.getjavajob.training.webproject1803.balchevskiyn.friends;

import com.getjavajob.training.webproject1803.balchevskiyn.service.FriendRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
@RequestMapping(value = "/friend-request-handler")
public class FriendRequestsHandlerController {

    private static final Logger logger = LoggerFactory.getLogger(FriendRequestsHandlerController.class);
    @Autowired
    private FriendRequestService friendRequestService;

    @RequestMapping(params = {"approveRequestFrom"})
    protected String approveFriendRequestFrom(
            @SessionAttribute("userId") int userId,
            @RequestParam("approveRequestFrom") int approveRequestFrom) {
        friendRequestService.approveFriendRequest(approveRequestFrom, userId);
        return "redirect:friend-requests";
    }

    @RequestMapping(params = {"disapproveRequestFrom"})
    protected String disapproveFriendRequestFrom(
            @SessionAttribute("userId") int userId,
            @RequestParam("disapproveRequestFrom") int disapproveRequestFrom) {
        friendRequestService.deleteFriendRequest(disapproveRequestFrom, userId);
        return "redirect:friend-requests";
    }

    @RequestMapping(params = {"disapproveRequestTo"})
    protected String disapproveFriendRequestTo(
            @SessionAttribute("userId") int userId,
            @RequestParam("disapproveRequestTo") int disapproveRequestTo) {
        friendRequestService.deleteFriendRequest(userId, disapproveRequestTo);
        return "redirect:friend-requests";
    }
}
