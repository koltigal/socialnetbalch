package com.getjavajob.training.webproject1803.balchevskiyn.friends;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.service.AccountService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/friends")
public class FriendsPageController {

    private static final Logger logger = LoggerFactory.getLogger(FriendsPageController.class);
    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.GET)
    protected ModelAndView showFriendsList(@SessionAttribute("userId") int userId) {
        List<Account> friendsList = accountService.getFriendsList(userId);
        ModelAndView modelAndView = new ModelAndView("friends/friendsPage");
        modelAndView.addObject("friendsList", friendsList);
        return modelAndView;
    }
}
