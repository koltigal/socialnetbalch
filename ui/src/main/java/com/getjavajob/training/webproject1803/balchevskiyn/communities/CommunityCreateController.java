package com.getjavajob.training.webproject1803.balchevskiyn.communities;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.Util.printMessageAndRedirect;

import com.getjavajob.training.webproject1803.balchevskiyn.model.CommunityUiWrapper;
import com.getjavajob.training.webproject1803.balchevskiyn.service.CommunityService;
import java.io.IOException;
import javax.servlet.http.HttpServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/create-community")
public class CommunityCreateController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(CommunityCreateController.class);
    @Autowired
    private CommunityService communityService;

    @RequestMapping(method = RequestMethod.GET)
    protected ModelAndView showCreateCommunityPage() {
        return new ModelAndView("communities/createCommunityPage");
    }

    @RequestMapping(method = RequestMethod.POST, params = {"communityName", "description"})
    protected ModelAndView createCommunity(
            @SessionAttribute("userId") int userId, @ModelAttribute CommunityUiWrapper communityUiWrapper,
            @RequestParam("avatar") MultipartFile avatar) throws IOException {
        int newCommunityId =
                communityService.createCommunity(communityUiWrapper.getCommunity(userId));
        if (avatar.getSize() > 0) {
            communityService.updateAvatarById(newCommunityId, avatar.getBytes());
        }
        return printMessageAndRedirect("user-page?pageUserId=" + userId, "Сообщество создано!");
    }
}
