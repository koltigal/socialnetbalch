package com.getjavajob.training.webproject1803.balchevskiyn.messages;

import com.getjavajob.training.webproject1803.balchevskiyn.service.AccountService;
import com.getjavajob.training.webproject1803.balchevskiyn.service.MessageService;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/personal-chat")
public class PersonalChatController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(PersonalChatController.class);
    @Autowired
    private MessageService messageService;
    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showChat(
            @SessionAttribute(value = "userId") int userId,
            @RequestParam("accountWithChatId") int accountWithChatId) {
        ModelAndView modelAndView = new ModelAndView("messages/socketChatPage");
        List<RootMessage> chat = messageService.getAllMessagesBetweenTwoAccountsByType(userId,
                accountWithChatId, PersonalMessage.class);
        modelAndView.addObject("chat", chat);
        modelAndView.addObject("senderAccount", accountService.getAccountById(userId));
        modelAndView.addObject("receiverAccount", accountService.getAccountById(accountWithChatId));
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String postPersonalMessage(
            @RequestParam("accountWithChatId") int accountWithChatId,
            @SessionAttribute("userId") int userId, @RequestParam("personalMessageText") String personalMessageText,
            @RequestParam("photo") MultipartFile photo) throws IOException {
        if (!personalMessageText.isEmpty()) {
            int newMessageId = messageService.addMessageBySenderReceiverAndType(userId, accountWithChatId,
                    personalMessageText, PersonalMessage.class);
            if (photo.getSize() > 0) {
                messageService.insertMessageImageById(newMessageId, photo.getBytes(), PersonalMessage.class);
            }
        } else {
            if (photo.getSize() > 0) {
                int newMessageId = messageService.addMessageBySenderReceiverAndType(userId, accountWithChatId,
                        personalMessageText, PersonalMessage.class);
                messageService.insertMessageImageById(newMessageId, photo.getBytes(), PersonalMessage.class);
            }
        }
        return "redirect:personal-chat?accountWithChatId=" + accountWithChatId;
    }
}
