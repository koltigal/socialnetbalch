package com.getjavajob.training.webproject1803.balchevskiyn.users;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.Util.printMessageAndRedirect;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.Account.AccountRole;
import com.getjavajob.training.webproject1803.balchevskiyn.service.AccountService;
import java.io.IOException;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/edit-user-data")
public class UserEditController {

    private static final Logger logger = LoggerFactory.getLogger(UserEditController.class);
    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showUserSettings(
            @SessionAttribute("userId") int userId, @RequestParam("userToEditId") int userToEditId) {
        Account personalAccount = accountService.getAccountById(userId);
        ModelAndView modelAndView = new ModelAndView("users/userEditPage");
        if (userId == userToEditId) {
            modelAndView.addObject("userToEditAccount", personalAccount);
        } else {
            if (personalAccount.getRole().equals(AccountRole.ADMIN)) {
                modelAndView.addObject("userToEditAccount", accountService.getAccountById(userToEditId));
            } else {
                return printMessageAndRedirect(
                        "user-page?pageUserId=" + userId,
                        "Вы не являетесь администратором!");
            }
        }
        return modelAndView;
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(method = RequestMethod.POST, params = {"deleteAccount"})
    public ModelAndView deleteAccount(
            @RequestParam("userToEditId") int userToEditId) {
        accountService.deleteAccount(userToEditId);
        return printMessageAndRedirect("logout", "Аккаунт удален!");
    }

    @RequestMapping(method = RequestMethod.POST, params = {"password"})
    public ModelAndView changePassword(
            @RequestParam("userToEditId") int userToEditId, @RequestParam("password") String password) {
        accountService.updatePassword(userToEditId, password);
        return printMessageAndRedirect("user-page?pageUserId=" + userToEditId, "Пароль изменен!");
    }

    @RequestMapping(method = RequestMethod.POST, params = {"email"})
    public ModelAndView changeSettings(
            @RequestParam("userToEditId") int userToEditId,
            @RequestParam("avatar") MultipartFile avatarPart, @RequestParam("email") String email,
            @RequestParam("name") String name, @RequestParam(value = "role") String role,
            @RequestParam("birthDate") String birthDateString, @RequestParam("skype") String skype,
            @RequestParam("icq") String icqString, @RequestParam(value = "phone", required = false) String[] phone,
            HttpSession session)
            throws IOException {
        byte[] avatar;
        if (avatarPart.getSize() > 0) {
            avatar = avatarPart.getBytes();
        } else {
            avatar = accountService.getAvatarById(userToEditId);
        }
        Date birthDate = birthDateString.isEmpty() ? null : Date.valueOf(birthDateString);
        skype = skype.isEmpty() ? null : skype;
        Integer icq = icqString.isEmpty() ? null : Integer.valueOf(icqString);
        List<String> phones = phone == null ? null : Arrays.asList(phone);
        accountService.updateAccount(avatar, userToEditId, name, birthDate, email, icq, skype,
                AccountRole.valueOf(role), phones);
        session.setAttribute("userRole", role);
        session.setAttribute("userName", name);
        return printMessageAndRedirect("user-page?pageUserId=" + userToEditId, "Данные изменены!");

    }

    @RequestMapping(method = RequestMethod.GET, params = {"getAccountXML"}, produces = "application/xml")
    @ResponseBody
    public Account getAccountXML(@RequestParam("userId") int userId, HttpServletResponse resp) {
        resp.setHeader("Content-Disposition", "attachment; filename=\"accountXML.xml\"");
        return accountService.getAccountById(userId);
    }

    @RequestMapping(method = RequestMethod.POST, params = {"setAccountXML"})
    public ModelAndView setAccountXML(
            @RequestParam("xmlAccount") MultipartFile accountXml, @RequestParam("userId") int userId)
            throws IOException {
        ModelAndView modelAndView = new ModelAndView("users/userEditPage");
        try {
            JAXBContext jc = JAXBContext.newInstance(Account.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            Account account = (Account) unmarshaller.unmarshal(accountXml.getInputStream());
            if (account.getId() != userId) {
                throw new IllegalStateException("Id in uploaded file should be equal to current user id");
            } else {
                modelAndView.addObject("userToEditAccount", account);
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return modelAndView;
    }
}
