package com.getjavajob.training.webproject1803.balchevskiyn.communities;

import com.getjavajob.training.webproject1803.balchevskiyn.service.CommunityService;
import javax.servlet.http.HttpServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/communities")
public class CommunitiesPageController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(CommunitiesPageController.class);
    @Autowired
    private CommunityService communityService;

    @RequestMapping(method = RequestMethod.GET)
    protected ModelAndView showCommunitiesList(@SessionAttribute("userId") int userId) {
        ModelAndView modelAndView = new ModelAndView("communities/communitiesPage");
        modelAndView.addObject("groupsList", communityService.getCommunitiesListByUserId(userId));
        return modelAndView;
    }
}
