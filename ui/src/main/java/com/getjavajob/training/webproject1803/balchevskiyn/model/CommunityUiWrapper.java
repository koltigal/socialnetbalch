package com.getjavajob.training.webproject1803.balchevskiyn.model;

import com.getjavajob.training.webproject1803.balchevskiyn.Community;
import java.time.LocalDate;

public class CommunityUiWrapper {

    private String communityName;
    private String description;

    public CommunityUiWrapper(String communityName, String description) {
        this.communityName = communityName;
        this.description = description;
    }

    public Community getCommunity(int creatorId) {
        return new Community(communityName, true, LocalDate.now(), description, creatorId);
    }
}
