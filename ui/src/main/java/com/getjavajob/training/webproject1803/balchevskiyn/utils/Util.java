package com.getjavajob.training.webproject1803.balchevskiyn.utils;

import org.springframework.web.servlet.ModelAndView;

public class Util {

    public static ModelAndView printMessageAndRedirect(String url, String message) {
        ModelAndView modelAndView = new ModelAndView("common/redirectPage");
        modelAndView.addObject("redirectUrl", url);
        modelAndView.addObject("message", message);
        return modelAndView;
    }
}
