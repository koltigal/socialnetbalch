package com.getjavajob.training.webproject1803.balchevskiyn.communities;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.Util.printMessageAndRedirect;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.CommunityUser.CommunityUserRole;
import com.getjavajob.training.webproject1803.balchevskiyn.service.CommunityService;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/community-administration-page")
public class CommunityAdminController {

    private static final Logger logger = LoggerFactory.getLogger(CommunityAdminController.class);
    @Autowired
    private CommunityService communityService;

    @RequestMapping(method = RequestMethod.GET)
    protected ModelAndView showCommunityAdminPage(
            @SessionAttribute("userId") int userId,
            @RequestParam("communityId") int communityId) {
        List<Account> communityAdmins =
                communityService.getAllUsersByRoleFromCommunity(communityId, CommunityUserRole.ADMIN);
        if (!userIsAdmin(userId, communityAdmins)) {
            return printMessageAndRedirect(
                    "user-page?pageUserId=" + userId,
                    "Вы не являетесь администратором данного сообщества!");
        } else {
            List<Account> communityUsers = communityService
                    .getAllUsersByRoleFromCommunity(communityId, CommunityUserRole.USER);
            ModelAndView modelAndView = new ModelAndView("communities/communityAdminPage");
            modelAndView.addObject("community", communityService.getCommunityById(communityId));
            modelAndView.addObject("communityAdmins", communityAdmins);
            modelAndView.addObject("communityUsers", communityUsers);
            return modelAndView;
        }
    }

    @RequestMapping(method = RequestMethod.POST, params = {"deleteCommunity"})
    protected ModelAndView deleteCommunity(
            @RequestParam("communityId") int communityId, @SessionAttribute("userId") int userId) {
        communityService.deleteCommunity(communityId);
        return printMessageAndRedirect("user-page?pageUserId=" + userId, "Сообщество удалено!");
    }

    @RequestMapping(method = RequestMethod.POST, params = {"makeCommunityAdminUserId", "makeCommunityAdminCommunityId"})
    protected String makeUserCommunityAdmin(
            @RequestParam("makeCommunityAdminUserId") int makeCommunityAdminUserId,
            @RequestParam("makeCommunityAdminCommunityId") int makeCommunityAdminCommunityId) {
        communityService.makeUserCommunityAdmin(makeCommunityAdminUserId, makeCommunityAdminCommunityId);
        return "redirect:community-administration-page?communityId=" + makeCommunityAdminCommunityId;
    }

    @RequestMapping(method = RequestMethod.POST, params = {"deleteFromCommunityUserId",
            "deleteFromCommunityCommunityId"})
    protected String deleteUserFromCommunity(
            @RequestParam("deleteFromCommunityUserId") int deleteFromCommunityUserId,
            @RequestParam("deleteFromCommunityCommunityId") int deleteFromCommunityCommunityId) {
        communityService.deleteCommunityUser(deleteFromCommunityUserId, deleteFromCommunityCommunityId);
        return "redirect:community-administration-page?communityId=" + deleteFromCommunityCommunityId;
    }

    @RequestMapping(method = RequestMethod.POST, params = {"changeAvatar"})
    protected String changeAvatar(
            @RequestParam("communityId") int communityId, @RequestParam("avatar") MultipartFile avatar)
            throws IOException {
        if (avatar.getSize() > 0) {
            communityService.updateAvatarById(communityId, avatar.getBytes());
        }
        return "redirect:community-administration-page?communityId=" + communityId;
    }

    private boolean userIsAdmin(int userId, List<Account> communityAdmins) {
        for (Account communityAdmin : communityAdmins) {
            if (communityAdmin.getId() == userId) {
                return true;
            }
        }
        return false;
    }
}
