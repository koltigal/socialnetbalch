package com.getjavajob.training.webproject1803.balchevskiyn.users;

import com.getjavajob.training.webproject1803.balchevskiyn.service.AccountService;
import com.getjavajob.training.webproject1803.balchevskiyn.service.FriendRequestService;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/user-page")
public class UserController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private FriendRequestService friendRequestService;
    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showUserPage(
            @RequestParam("pageUserId") int pageUserId,
            @SessionAttribute(value = "userId", required = false) int userId, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("users/userPage");
        modelAndView.addObject("pageUserAccount", accountService.getAccountById(pageUserId));
        if (pageUserId != userId) {
            if (accountService.checkAccountsAreFriends(userId, pageUserId)) {
                modelAndView.addObject("accountsAreFriends", true);
                modelAndView.addObject("existFriendRequest", false);
            } else {
                modelAndView.addObject("accountsAreFriends", false);
                modelAndView.addObject(
                        "existFriendRequest",
                        friendRequestService.checkRequestByAccountsId(userId, pageUserId));
            }
        }
        session.setAttribute("receiverId", pageUserId);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST, params = {"deleteFromFriendsId"})
    public String deleteFromFriends(
            @SessionAttribute("userId") int userId,
            @RequestParam("deleteFromFriendsId") int deleteFromFriendsId) {
        accountService.deleteFriend(userId, deleteFromFriendsId);
        return "redirect:user-page?pageUserId=" + deleteFromFriendsId;
    }

    @RequestMapping(method = RequestMethod.POST, params = {"addToFriendsId"})
    public String addToFriends(
            @SessionAttribute("userId") int userId,
            @RequestParam("addToFriendsId") int addToFriendsId) {
        friendRequestService.addFriendRequest(userId, addToFriendsId);
        return "redirect:user-page?pageUserId=" + addToFriendsId;
    }
}
