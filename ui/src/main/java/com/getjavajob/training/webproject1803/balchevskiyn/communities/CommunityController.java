package com.getjavajob.training.webproject1803.balchevskiyn.communities;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.Util.printMessageAndRedirect;

import com.getjavajob.training.webproject1803.balchevskiyn.service.CommunityRequestService;
import com.getjavajob.training.webproject1803.balchevskiyn.service.CommunityService;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/community")
public class CommunityController {

    private static final Logger logger = LoggerFactory.getLogger(CommunityController.class);
    @Autowired
    private CommunityRequestService communityRequestService;
    @Autowired
    private CommunityService communityService;

    @RequestMapping(method = RequestMethod.GET)
    protected ModelAndView showCommunity(
            @SessionAttribute("userId") int userId,
            @RequestParam("communityId") int communityId, HttpSession session) {
        String redirectUrl = "user-page?pageUserId=" + userId;
        if (communityService.getCommunityById(communityId) == null) {
            return printMessageAndRedirect(redirectUrl, "Нет такого сообщества!");
        } else if (communityService.checkUserInsideCommunity(userId, communityId)) {
            ModelAndView modelAndView = new ModelAndView("communities/communityPage");
            modelAndView.addObject("community", communityService.getCommunityById(communityId));
            modelAndView
                    .addObject("adminAttribute", communityService.checkUserIsAdminInCommunity(userId, communityId));
            session.setAttribute("receiverId", communityId);
            return modelAndView;
        } else if (communityRequestService.checkRequestByUserAndCommunityId(userId, communityId)) {
            return printMessageAndRedirect(redirectUrl, "Вы уже подали заявку, ждите рассмотрения!");
        } else {
            ModelAndView modelAndView = new ModelAndView("communities/userNotInCommunityPage");
            modelAndView.addObject("community", communityService.getCommunityById(communityId));
            return modelAndView;
        }
    }

    @RequestMapping(method = RequestMethod.GET, params = {"requestedCommunityId"})
    protected ModelAndView addCommunityRequest(
            @SessionAttribute("userId") int userId,
            @RequestParam("requestedCommunityId") int requestedCommunityId) {
        communityRequestService.addCommunityRequest(userId, requestedCommunityId);
        return printMessageAndRedirect("user-page?pageUserId=" + userId, "Заявка подана!");
    }

    @RequestMapping(method = RequestMethod.POST, params = {"leaveCommunity"})
    protected ModelAndView leaveGroup(
            @SessionAttribute("userId") int userId,
            @RequestParam("communityId") int communityId) {
        communityService.deleteCommunityUser(userId, communityId);
        return printMessageAndRedirect("user-page?pageUserId=" + userId, "Вы вышли из группы!");
    }
}
