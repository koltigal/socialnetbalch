package com.getjavajob.training.webproject1803.balchevskiyn.messages;

import com.getjavajob.training.webproject1803.balchevskiyn.service.AccountService;
import com.getjavajob.training.webproject1803.balchevskiyn.service.MessageService;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketChatController {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketChatController.class);
    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private MessageService messageService;
    @Autowired
    private AccountService accountService;

    @MessageMapping("/chat")
    public void send(final ChatMessage message, Principal principal) {
        int receiverId = accountService.getIdByEmail(message.getToEmail());
        messageService.addMessageBySenderReceiverAndType(message.getFromId(), receiverId,
                message.getText(), PersonalMessage.class);
        String time = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
        template.convertAndSendToUser(message.getToEmail(), "/topic/messages",
                new ChatOutputMessage(message.getFromId(), message.getFromName(), message.getFromEmail(),
                        message.getText(), time));
        template.convertAndSendToUser(principal.getName(), "/topic/messages",
                new ChatOutputMessage(message.getFromId(), message.getFromName(), message.getFromEmail(),
                        message.getText(), time));
    }
}
