package com.getjavajob.training.webproject1803.balchevskiyn.communities;

import com.getjavajob.training.webproject1803.balchevskiyn.messages.CommunityMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.RootMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.service.MessageService;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/community-wall")
public class CommunityWallController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(CommunityWallController.class);
    @Autowired
    private MessageService messageService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showCommunityWall(@SessionAttribute("receiverId") int receiverId) {
        ModelAndView modelAndView = new ModelAndView("communities/communityWallPage");
        List<RootMessage> wallChat = messageService.getMessagesByReceiverAndType(receiverId, CommunityMessage.class);
        modelAndView.addObject("wallChat", wallChat);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String postMessageOnCommunityWall(
            @SessionAttribute("receiverId") int receiverId,
            @SessionAttribute("userId") int userId, @RequestParam("wallMessageText") String wallMessageText,
            @RequestParam("photo") MultipartFile photo) throws IOException {
        if (!wallMessageText.isEmpty()) {
            int newMessageId = messageService.addMessageBySenderReceiverAndType(userId, receiverId, wallMessageText,
                    CommunityMessage.class);
            if (photo.getSize() > 0) {
                messageService.insertMessageImageById(newMessageId, photo.getBytes(), CommunityMessage.class);
            }
        } else if (photo.getSize() > 0) {
            int newMessageId = messageService.addMessageBySenderReceiverAndType(userId, receiverId, wallMessageText,
                    CommunityMessage.class);
            messageService.insertMessageImageById(newMessageId, photo.getBytes(), CommunityMessage.class);
        }
        return "redirect:community?communityId=" + receiverId;
    }
}
