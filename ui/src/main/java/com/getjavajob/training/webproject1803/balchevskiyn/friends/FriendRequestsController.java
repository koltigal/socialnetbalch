package com.getjavajob.training.webproject1803.balchevskiyn.friends;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.FriendRequest;
import com.getjavajob.training.webproject1803.balchevskiyn.service.FriendRequestService;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/friend-requests")
public class FriendRequestsController {

    private static final Logger logger = LoggerFactory.getLogger(FriendRequestsController.class);
    @Autowired
    private FriendRequestService friendRequestService;

    @RequestMapping(method = RequestMethod.GET)
    protected ModelAndView showFriendReuests(@SessionAttribute("userId") int userId) {
        ModelAndView modelAndView = new ModelAndView("friends/friendRequestsPage");
        List<FriendRequest> friendRequestsToList = friendRequestService.getFriendRequestsToAccount(userId);
        List<FriendRequest> friendRequestsFromList = friendRequestService.getFriendRequestsFromAccount(userId);

        List<Account> accountsRequestedToThis = new ArrayList<>();
        if (friendRequestsToList != null) {
            for (FriendRequest request : friendRequestsToList) {
                accountsRequestedToThis.add(request.getRequestingUser());
            }
        }
        modelAndView.addObject("accountsRequestedToThis", accountsRequestedToThis);

        List<Account> accountsRequestedFromThis = new ArrayList<>();
        if (friendRequestsFromList != null) {
            for (FriendRequest request : friendRequestsFromList) {
                accountsRequestedFromThis.add(request.getRequestedUser());
            }
        }
        modelAndView.addObject("accountsRequestedFromThis", accountsRequestedFromThis);
        return modelAndView;
    }
}
