package com.getjavajob.training.webproject1803.balchevskiyn.search;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.Community;
import com.getjavajob.training.webproject1803.balchevskiyn.service.AccountService;
import com.getjavajob.training.webproject1803.balchevskiyn.service.CommunityService;
import java.util.List;
import javax.servlet.http.HttpServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/search")
public class SearchController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
    @Autowired
    private CommunityService communityService;
    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.GET, params = {"searchWord"})
    public ModelAndView showSearchResult(@RequestParam("searchWord") String searchWord) {
        ModelAndView modelAndView = new ModelAndView("common/searchResultPage");
        List<Account> foundAccounts = accountService.getAccountsByName(searchWord);
        List<Community> foundCommunities = communityService.getCommunitiesListByName(searchWord);
        modelAndView.addObject("foundAccounts", foundAccounts);
        modelAndView.addObject("foundCommunities", foundCommunities);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, params = {"accountSearch", "ajaxSearchWord"})
    @ResponseBody
    public List<Account> accountSearch(@RequestParam("ajaxSearchWord") String searchWord) {
        return accountService.getAccountsByName(searchWord);
    }

    @RequestMapping(method = RequestMethod.GET, params = {"communitySearch", "ajaxSearchWord"})
    @ResponseBody
    public List<Community> communitySearch(@RequestParam("ajaxSearchWord") String searchWord) {
        return communityService.getCommunitiesListByName(searchWord);
    }
}
