package com.getjavajob.training.webproject1803.balchevskiyn.messages;

import com.getjavajob.training.webproject1803.balchevskiyn.service.AccountService;
import javax.servlet.http.HttpServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/messages")
public class MessagesPageController extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(MessagesPageController.class);
    @Autowired
    private AccountService accountService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showMessages(@SessionAttribute(value = "userId") int userId) {
        ModelAndView modelAndView = new ModelAndView("messages/messagesPage");
        modelAndView.addObject("accountsWithChat", accountService.getUsersWithWhichHaveChat(userId));
        return modelAndView;
    }
}
