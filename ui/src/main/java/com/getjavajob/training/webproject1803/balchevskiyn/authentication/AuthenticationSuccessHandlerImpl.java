package com.getjavajob.training.webproject1803.balchevskiyn.authentication;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.service.AccountService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessHandlerImpl.class);
    @Autowired
    AccountService accountService;

    public static boolean isRememberMeAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return false;
        }
        return RememberMeAuthenticationToken.class.isAssignableFrom(authentication.getClass());
    }

    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {
        Integer userId = accountService.getIdByEmail(authentication.getName());
        setSessionAttributes(request.getSession(), accountService.getAccountById(userId));
        if (isRememberMeAuthenticated()) {
            request.getRequestDispatcher("/WEB-INF/jsp/common/sessionExpired.jsp").forward(request, response);
        } else {
            response.sendRedirect("user-page?pageUserId=" + userId);
        }
    }

    private void setSessionAttributes(HttpSession session, Account userAccount) {
        session.setAttribute("userId", userAccount.getId());
        session.setAttribute("userRole", userAccount.getStringRole());
        session.setAttribute("userName", userAccount.getName());
    }

}