package com.getjavajob.training.webproject1803.balchevskiyn.communities;

import com.getjavajob.training.webproject1803.balchevskiyn.service.CommunityRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/community-request-handler")
public class CommunityRequestsHandlerController {

    private static final Logger logger = LoggerFactory.getLogger(CommunityRequestsHandlerController.class);
    @Autowired
    private CommunityRequestService communityService;

    @RequestMapping(method = RequestMethod.POST, params = {"approve"})
    protected String approveRequest(
            @RequestParam("requestingUserId") int requestingUserId, @RequestParam("communityId") int communityId) {
        communityService.approveCommunityRequest(requestingUserId, communityId);
        return "redirect:community-requests";
    }

    @RequestMapping(method = RequestMethod.POST, params = {"disapprove"})
    protected String dissaproveRequest(
            @RequestParam("requestingUserId") int requestingUserId, @RequestParam("communityId") int communityId) {
        communityService.deleteCommunityRequest(requestingUserId, communityId);
        return "redirect:community-requests";
    }
}
