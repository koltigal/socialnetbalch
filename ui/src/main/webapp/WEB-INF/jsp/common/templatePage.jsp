<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<div class="d-flex flex-column section col-6 mx-auto">
    <c:out value="${message}"></c:out>
</div>

</body>
</html>