<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/css; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">

    <!-- My own CSS-->
    <link href="${pageContext.request.contextPath}/resources/css/userPageStyle.css" rel="stylesheet" type="text/css"/>
    <!-- JQuery UI CSS-->
    <link href="${pageContext.request.contextPath}/resources/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.14/css/mdb.min.css" rel="stylesheet">

    <!-- Bootstrap tooltips -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"
            type="text/javascript"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"
            type="text/javascript"></script>
    <!-- MDB core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.14/js/mdb.min.js"
            type="text/javascript"></script>
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- JQuery UI -->
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui.min.js"></script>
    <!-- SockJS -->
    <script src="${pageContext.request.contextPath}/resources/js/sockjs-0.3.4.js"></script>
    <!-- STOMP -->
    <script src="${pageContext.request.contextPath}/resources/js/stomp.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/ui.js"></script>

</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark blue my-4">

    <a class="navbar-brand waves-effect" href="user-page?pageUserId=${userId}">SocNet</a>
    <ul class="navbar-nav">
        <li class="nav-item"><a class="nav-link" href="friends">Список друзей</a></li>
        <li class="nav-item"><a class="nav-link" href="friend-requests">Запросы друзей</a></li>
        <li class="nav-item"><a class="nav-link" href="communities">Группы</a></li>
        <li class="nav-item"><a class="nav-link" href="community-requests">Запросы групп</a></li>
        <li class="nav-item"><a class="nav-link" href="messages">Сообщения</a></li>
        <li class="nav-item"><a class="nav-link" href="edit-user-data?userToEditId=${userId}">Настройки</a></li>
    </ul>

    <form action="search" class="form-inline ml-auto">
        <div class="md-form my-0">
            <input aria-label="Search" class="form-control mr-sm-2" id="searchWord" name="searchWord"
                   placeholder="Search"
                   type="text"/>
        </div>
        <button class="btn btn-outline-white px-3" id="searchButton" type="submit"><i aria-hidden="true"
                                                                                      class="fa fa-search"></i>
        </button>
    </form>

    <form action="logout" class="form-inline ml-auto" method="post">
        <div class="md-form my-0">
            <span class="text-white">${userName}</span>
        </div>
        <button class="btn btn-outline-white px-3" type="submit"><i aria-hidden="true" class="fa fa-sign-out"></i>
        </button>
    </form>

</nav>
</br>
</body>
</html>