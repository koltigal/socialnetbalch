<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>
<script src="${pageContext.request.contextPath}/resources/js/ajaxSearch.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>

<div class="d-flex flex-column mb-4 col-5 text-center section mx-auto" id="foundAccountsSection">
    <p class="h4 mb-2">Список найденных аккаунтов</p>
    <table id="foundAccountsTable">
    </table>
</div>

<div class="d-flex flex-column mb-4 col-5 text-center section mx-auto" id="foundCommunitiesSection">
    <p class="h4 mb-2">Список найденных групп</p>
    <table id="foundCommunitiesTable">
    </table>
</div>

</body>
</html>