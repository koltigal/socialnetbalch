<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@page contentType="text/html; charset=UTF-8" %>
<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>
<div class="wrapper">
    <h2>Сессия была просрочена, пожалуйста попробуйте ещё раз.</h2>
    <a href="user-page?pageUserId=${userId}">Personal Page</a>
</div>
</body>
</html>
