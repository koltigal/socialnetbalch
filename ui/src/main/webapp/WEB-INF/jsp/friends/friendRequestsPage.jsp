<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<div class="d-flex justify-content-center">
    <div class="col-9 section text-center">
        <p class="h4 mb-2">Запросы на добавление к вам в друзья</p>
        <table class="col-12">
            <c:choose>
                <c:when test="${not empty accountsRequestedToThis}">
                    <c:forEach items="${accountsRequestedToThis}" var="accountRequestedTo">
                        <tr>
                            <td>
                                <div class="d-flex border-top border-light justify-content-center">
                                    <div class="mb-2 col-2 text-right align-self-center">
                                        <a href="user-page?pageUserId=${accountRequestedTo.id}">
                                            <img class="img-fluid rounded-circle hoverable"
                                                 height="50"
                                                 onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/default-avatar-250x250.png';"
                                                 src="images?imageType=personAvatar&imageId=${accountRequestedTo.id}"
                                                 width="50">
                                        </a>
                                    </div>
                                    <div class="col-3 text-left align-self-center">
                                        <b>
                                            <a href="user-page?pageUserId=${accountRequestedTo.id}">${accountRequestedTo.name}</a>
                                        </b>
                                    </div>
                                    <div class="col-5 d-flex justify-content-around">
                                        <form action="friend-request-handler" method="post">
                                            <input name="approveRequestFrom" type="hidden"
                                                   value="${accountRequestedTo.id}"/>
                                            <input class="btn btn-info btm-sm my-1" type="submit"
                                                   value="Подтвердить"/>
                                        </form>
                                        <form action="friend-request-handler" method="post">
                                            <input name="disapproveRequestFrom" type="hidden"
                                                   value="${accountRequestedTo.id}"/>
                                            <input class="btn btn-info btm-sm my-1" type="submit" value="Отклонить"/>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <tr>
                        <td>
                            <p class="border-top border-light text-center">Нет запросов</p>
                        </td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </table>
    </div>
</div>

<div class="d-flex justify-content-center">
    <div class="col-9 section text-center">
        <p class="h4 mb-2">Ваши запросы на добавление в друзья</p>
        <table class="col-12">
            <c:choose>
                <c:when test="${not empty accountsRequestedFromThis}">
                    <c:forEach items="${accountsRequestedFromThis}" var="accountRequestedFrom">
                        <tr>
                            <td>
                                <div class="d-flex border-top border-light justify-content-center">
                                    <div class="mb-2 col-2 text-right align-self-center">
                                        <a href="user-page?pageUserId=${accountRequestedFrom.id}">
                                            <img class="img-fluid rounded-circle hoverable"
                                                 height="50"
                                                 onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/default-avatar-250x250.png';"
                                                 src="images?imageType=personAvatar&imageId=${accountRequestedFrom.id}"
                                                 width="50">
                                        </a>
                                    </div>
                                    <div class="col-3 text-left align-self-center">
                                        <b>
                                            <a href="user-page?pageUserId=${accountRequestedFrom.id}">${accountRequestedFrom.name}</a>
                                        </b>
                                    </div>
                                    <div class="col-5 d-flex justify-content-center">
                                        <form action="friend-request-handler" method="post">
                                            <input name="disapproveRequestTo" type="hidden"
                                                   value="${accountRequestedFrom.id}"/>
                                            <input class="btn btn-info btm-sm my-1" type="submit"
                                                   value="Отозвать заявку"/>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <tr>
                        <td><p class="border-top border-light text-center">Нет запросов</p></td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </table>
    </div>
</div>

</body>
</html>