<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<div class="d-flex flex-column mb-4 col-5 text-center section mx-auto">
    <p class="h4 mb-2">Список друзей</p>
    <table>
        <c:choose>
            <c:when test="${not empty friendsList}">
                <c:forEach items="${friendsList}" var="friend">
                    <tr>
                        <td>
                            <div class="d-flex mb-2 border-top border-light">
                                <div class="col-2 ">
                                    <a href="user-page?pageUserId=${friend.id}">
                                        <img class="img-fluid rounded-circle hoverable"
                                             height="50"
                                             onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/default-avatar-250x250.png';"
                                             src="images?imageType=personAvatar&imageId=${friend.id}" width="50">
                                    </a>
                                </div>
                                <div class="col-10 text-left align-self-center">
                                    <b>
                                        <a href="user-page?pageUserId=${friend.id}">${friend.name}</a>
                                    </b>
                                </div>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <tr>
                    <td><p>Вы пока не добавили ни одного друга</p></td>
                </tr>
            </c:otherwise>
        </c:choose>
    </table>
</div>

</body>
</html>