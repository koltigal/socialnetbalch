<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<script src="${pageContext.request.contextPath}/resources/js/webSocketStomp.js"></script>
<script>
var senderAccountId ="${senderAccount.id}";
var senderAccountName ="${senderAccount.name}";
var senderAccountEmail ="${senderAccount.email}";
var receiverAccountEmail ="${receiverAccount.email}";
var pageAddress ="${pageContext.request.contextPath}";

</script>

<div class="section col-6 mx-auto">
    <div class="d-flex justify-content-around">
        <div class="shadow-textarea flex-fill">
            <textarea class="form-control z-depth-1" name="personalMessageText" id="messageText"
                      placeholder="Введите сообщение.."
                      rows="3"></textarea>
        </div>
        <div class="align-self-center">
            <input class="btn btn-info btn-sm m-2" id="sendMessage" type="submit" value="Отправить"/>
        </div>
    </div>
</div>

<table class="col-6 mx-auto" id="chatTable">
    <c:forEach items="${chat}" var="message">
        <tr>
            <td>
                <div class="section">
                    <div class="d-flex  mb-4">
                        <div class="col-2">
                            <a href="user-page?pageUserId=${message.sender.id}">
                                <img class="img-fluid rounded-circle hoverable"
                                     height="50"
                                     onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/default-avatar-250x250.png';"
                                     src="images?imageType=personAvatar&imageId=${message.sender.id}" width="50">
                            </a>
                        </div>
                        <div class="col-10 text-left align-self-center">
                            <b>
                                <a href="user-page?pageUserId=${message.sender.id}">${message.sender.name}</a>
                            </b><br>
                            <small>
                                <c:out value="${message.date}"></c:out>
                            </small>
                        </div>
                    </div>
                    <div class="col-12 text-left ">
                        <c:out value="${message.text}"></c:out>
                    </div>
                </div>
            </td>
        </tr>
    </c:forEach>
</table>

</body>
</html>