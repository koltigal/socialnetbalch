<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<div class="d-flex justify-content-center">
    <div class="col-5 section text-center">
        <p class="h4 mb-2">Сообщения</p>
        <table class="col-12">
            <c:choose>
                <c:when test="${not empty accountsWithChat}">
                    <c:forEach items="${accountsWithChat}" var="accountWithChat">
                        <tr>
                            <td>
                                <div class="d-flex border-top border-light justify-content-center">
                                    <div class="mb-2 col-7 align-self-center">
                                        <a href="user-page?pageUserId=${accountWithChat.id}">
                                            <img class="img-fluid rounded-circle hoverable"
                                                 height="50"
                                                 onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/default-avatar-250x250.png';"
                                                 src="images?imageType=personAvatar&imageId=${accountWithChat.id}"
                                                 width="50">
                                        </a>
                                        <b>
                                            <a href="user-page?pageUserId=${accountWithChat.id}">${accountWithChat.name}</a>
                                        </b>
                                    </div>
                                    <div class="col-5 d-flex justify-content-center">
                                        <form action="personal-chat">
                                            <input name="accountWithChatId" type="hidden"
                                                   value="${accountWithChat.id}"/>
                                            <input class="btn btn-info btm-sm my-1" type="submit"
                                                   value="Перейти к чату"/>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <tr>
                        <td><p class="border-top border-light text-center">У вас пока нет ни одного личного чата</p>
                        </td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </table>
    </div>
</div>
</body>
</html>