<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.Date"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@page contentType="text/html; charset=UTF-8" %>
<html xmlns="http://www.w3.org/1999/html">
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<div class="d-flex justify-content-center">
    <form action="edit-user-data" class="text-center border border-light p-5 my-2 col-5 section"
          enctype="multipart/form-data"
          method="post">
        <p class="h4 mb-2">Смена пароля</p>
        <input name="userToEditId" type="hidden" value="${userToEditAccount.id}"/>

        <input class="form-control mb-1" id="password" name="password" placeholder="Пароль" required
               title="Введите новый пароль" type="password"><br>

        <input class="form-control mb-1" id="confirmPassword" placeholder="Пароль повторно" required
               title="Введите пароль повторно" type="password"><br>

        <input class="btn btn-info btn-block my-1" id="savePassword" type="submit" value="Сохранить"/>
    </form>
</div>

<div class="d-flex justify-content-center">
    <form action="edit-user-data" class="text-center border border-light p-5 my-5 col-5 section"
          enctype="multipart/form-data" id="userEdit" method="post">
        <p class="h4 mb-4">Редактирование данных</p>
        <small class="form-text text-muted mb-1" id="settingsFormHelpBlock">
            Поля отмеченные звездочкой нельзя оставлять пустыми
        </small>

        <input name="userToEditId" type="hidden" value="${userToEditAccount.id}"/>

        <div class="d-flex mb-2">
            <div class="col-3 text-right align-self-center">
                <p class="text-muted">Email*: </p>
            </div>
            <div class="col-9 align-self-center">
                <input class="form-control mb-2" name="email" placeholder="E-mail*" required
                       type="email" value="${userToEditAccount.email}"/>
            </div>
        </div>

        <div class="d-flex mb-2">
            <div class="col-3 text-right align-self-center">
                <p class="text-muted">Имя*: </p>
            </div>
            <div class="col-9">
                <input class="form-control mb-2" name="name" required value="${userToEditAccount.name}"/>
            </div>
        </div>

        <div class="d-flex mb-2">
            <div class="col-3 text-right align-self-center">
                <p class="text-muted">Аватар: </p>
            </div>
            <div class="input-group col-9">
                <div class="custom-file">
                    <input aria-describedby="inputGroupFileAddon03" class="custom-file-input" id="inputGroupFile03"
                           name="avatar"
                           placeholder="Аватар"
                           type="file">
                    <label class="custom-file-label text-truncate text-muted text-left" for="inputGroupFile03"
                           style="font-family: sans-serif">Аватар</label>
                </div>
            </div>
        </div>

        <div class="d-flex mb-2">
            <div class="col-3 text-right align-self-center">
                <c:choose>
                    <c:when test="${userRole == 'ADMIN'}">
                        <p class="text-muted">Роль: </p>
                    </c:when>
                </c:choose>
            </div>
            <div class="col-9">
                <c:choose>
                    <c:when test="${userRole == 'ADMIN'}">
                        <select class="browser-default custom-select" form="userEdit" name="role">
                            <c:choose>
                                <c:when test="${userToEditAccount.stringRole == 'USER'}">
                                    <option selected value="USER">User</option>
                                    <option value="ADMIN">Admin</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="USER">User</option>
                                    <option selected value="ADMIN">Admin</option>
                                </c:otherwise>
                            </c:choose>
                        </select>
                        <br/>
                    </c:when>
                    <c:otherwise>
                        <input name="role" required type="hidden" value="${userToEditAccount.stringRole}"/><br>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>

        <div class="d-flex mb-2">
            <div class="col-3 text-right align-self-center">
                <p class="text-muted">Дата рождения: </p>
            </div>
            <div class="col-9">
                <input class="form-control mb-2" name="birthDate" type="date"
                       value="${userToEditAccount.stringBirthDate}"/>
            </div>
        </div>

        <div class="d-flex mb-2">
            <div class="col-3 text-right align-self-center">
                <p class="text-muted">Skype: </p>
            </div>
            <div class="col-9">
                <input class="form-control mb-2" name="skype" value="${userToEditAccount.skype}"/>
            </div>
        </div>

        <div class="d-flex mb-2">
            <div class="col-3 text-right align-self-center">
                <p class="text-muted">ICQ: </p>
            </div>
            <div class="col-9">
                <input class="form-control mb-2" name="icq" type="number" value="${userToEditAccount.icq}"/>
            </div>
        </div>

        <p class="border-top border-light">Мои телефоны:</p>

        <div class="d-flex mb-2">
            <div class="col-3 text-right align-self-center">
                <p class="text-muted">Новый телефон: </p>
            </div>
            <div class="input-group col-9 align-self-center">
                <input class="form-control" id="newPhone" title="Корректный формат +X XXX XXX XX XX без пробелов"
                       type="text"/>
                <div class="input-group-append">
                    <button class="btn btn-info m-0 px-3 py-2" id="addPhone" type="button">Добавить</button>
                </div>
            </div>
        </div>

        <div class="border-bottom border-light" id="phoneSection">
            <c:choose>
                <c:when test="${not empty userToEditAccount.phones}">
                    <c:forEach items="${userToEditAccount.phones}" var="phone">
                        <div class="d-flex mb-2">
                            <div class="col-3 text-right align-self-center">
                                <p class="text-muted">Телефон: </p>
                            </div>
                            <div class="input-group col-9 align-self-center">
                                <input class="form-control" name="phone" value="${phone}"/>
                                <div class="input-group-append">
                                    <button class="deletePhone btn btn-info m-0 px-3 py-2" type="button">Удалить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <p id="noPhonesAdded">Вы пока не добавили ни одного телефона</p>
                </c:otherwise>
            </c:choose>
        </div>

        <input class="btn btn-info btn-block my-1" id="saveUserSettings" type="submit" value="Сохранить изменения"/>
    </form>
</div>

<div class="d-flex justify-content-center">
    <form action="edit-user-data" class="text-center border border-light p-5 my-2 col-5 section">
        <p class="h4 mb-2">Выгрузка в XML</p>
        <input name="userId" type="hidden" value="${userToEditAccount.id}"/>
        <input name="getAccountXML" type="hidden" value="true"/>
        <input class="btn btn-info btn-block my-1" id="saveAccountXml" type="submit" value="Выгрузить в XML"/>
    </form>
</div>

<div class="d-flex justify-content-center">
    <form action="edit-user-data" class="text-center border border-light p-5 my-5 col-5 section"
          enctype="multipart/form-data" id="uploadAccountXml" method="post">
        <input name="userId" type="hidden" value="${userToEditAccount.id}"/>
        <input name="setAccountXML" type="hidden" value="true"/>
        <p class="h4 mb-2">Загрузка из XML</p>
        <div class="input-group col-9">
            <div class="custom-file">
                <input aria-describedby="inputGroupFileAddon04" class="custom-file-input" id="inputGroupFile04"
                       name="xmlAccount"
                       placeholder="XmlAccount"
                       type="file">
                <label class="custom-file-label text-truncate text-muted text-left" for="inputGroupFile04"
                       style="font-family: sans-serif">XmlAccount</label>
            </div>
        </div>
        <input class="btn btn-info btn-block my-1" id="setAccountXML" type="submit" value="Загрузить данные из файла"/>
    </form>
</div>

<div class="d-flex justify-content-center">
    <c:choose>
        <c:when test="${userRole == 'ADMIN' || userToEditAccount.id == userId}">
            <form action="edit-user-data" class="text-center border border-light p-5 my-2 col-5 section"
                  enctype="multipart/form-data" method="post">
                <p class="h4 mb-2">Удаление аккаунта</p>
                <input name="userToEditId" type="hidden" value="${userToEditAccount.id}"/>
                <input name="deleteAccount" type="hidden" value="true"/><br>
                <input class="btn btn-info btn-block my-1" id="deleteAccount" type="submit" value="Удалить аккаунт"/>
            </form>
        </c:when>
    </c:choose>
</div>

</body>
</html>
