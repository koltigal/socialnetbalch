<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<head>
    <link href="${pageContext.request.contextPath}/resources/css/userPageStyle.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<div class="d-flex justify-content-center">
    <div class="leftColumn col-3">
        <div class="section align-self-start">
            <img alt="avatar"
                 class="img-fluid d-block mx-auto"
                 onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/default-avatar-250x250.png';"
                 src="images?imageType=personAvatar&imageId=${pageUserAccount.id}">
            <c:choose>
                <c:when test="${not empty pageUserAccount.birthDate}">
                    <c:set value="${pageUserAccount.stringBirthDate}" var="birthDate"/>
                </c:when>
                <c:otherwise>
                    <c:set value="Не указана" var="birthDate"/>
                </c:otherwise>
            </c:choose>

            <p><b>Дата рождения: </b>
                <c:out value="${birthDate}"/>
            </p>

            <p><b>Email: </b>
                <c:out value="${pageUserAccount.email}"/>
            </p>

            <c:choose>
                <c:when test="${not empty pageUserAccount.icq}">
                    <c:set value="${pageUserAccount.icq}" var="icq"/>
                </c:when>
                <c:otherwise>
                    <c:set value="Не указан" var="icq"/>
                </c:otherwise>
            </c:choose>

            <p><b>Icq: </b>
                <c:out value="${icq}"/>
            </p>

            <c:choose>
                <c:when test="${not empty pageUserAccount.skype}">
                    <c:set value="${pageUserAccount.skype}" var="skype"/>
                </c:when>
                <c:otherwise>
                    <c:set value="Не указан" var="skype"/>
                </c:otherwise>
            </c:choose>
            <p><b>Skype: </b>
                <c:out value="${skype}"/>
            </p>


            <c:forEach items="${pageUserAccount.phones}" var="phone">
                <c:choose>
                    <c:when test="${not empty phone}">
                        <p><b>Телефон: </b>
                            <c:out value="${phone}"/>
                        </p>
                    </c:when>
                    <c:otherwise>
                        <p><b>Телефон: </b>
                            <c:out value="Не указан"/>
                        </p>
                    </c:otherwise>
                </c:choose>
            </c:forEach>


            <p><b>Роль: </b>
                <c:out value="${pageUserAccount.stringRole}"/>
            </p>

            <p><b>Дата создания: </b>
                <c:out value="${pageUserAccount.stringCreateDate}"/>
            </p>
        </div>
    </div>

    <div class="rightColumn col-6">
        <div class="section align-self-start text-center">
            <h4 class="my-2">
                <c:out value="${pageUserAccount.name}"/>
            </h4>
            <div class="d-flex justify-content-around">
                <c:choose>
                    <c:when test="${userId != pageUserAccount.id}">
                        <div>
                            <form action="personal-chat">
                                <input name="accountWithChatId" type="hidden" value="${pageUserAccount.id}"/>
                                <input class="btn btn-info btn-block mb-2" type="submit" value="Написать сообщение"/>
                            </form>
                        </div>
                        <c:choose>
                            <c:when test="${accountsAreFriends == true}">
                                <div>
                                    <form action="user-page?pageUserId=${userId}" method="post">
                                        <input name="deleteFromFriendsId" type="hidden" value="${pageUserAccount.id}"/>
                                        <input class="btn btn-info btn-block mb-2" type="submit"
                                               value="Удалить из друзей"/>
                                    </form>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <c:choose>
                                    <c:when test="${existFriendRequest == true}">
                                        <div>
                                            <button class="btn btn-info btn-block mb-2" disabled type="button">Заявка
                                                подана
                                            </button>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div>
                                            <form action="user-page?pageUserId=${userId}" method="post">
                                                <input name="addToFriendsId" type="hidden"
                                                       value="${pageUserAccount.id}"/>
                                                <input class="btn btn-info btn-block mb-2" type="submit"
                                                       value="Добавить в друзья"/>
                                            </form>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${userRole == 'ADMIN'}">
                                <div>
                                    <a class="btn btn-info btn-block mb-2"
                                       href="edit-user-data?userToEditId=${pageUserAccount.id}"
                                       role="button">Администрирование</a>
                                </div>
                            </c:when>
                        </c:choose>
                    </c:when>
                </c:choose>
            </div>
        </div>
        <jsp:include page="/user-wall"/>
    </div>
</div>

</body>
</html>