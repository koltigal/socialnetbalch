<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@page contentType="text/html; charset=UTF-8" %>
<html>
<meta charset="UTF-8">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">

    <!-- My own CSS-->
    <link href="${pageContext.request.contextPath}/resources/css/userPageStyle.css" rel="stylesheet" type="text/css"/>
    <!-- JQuery UI CSS-->
    <link href="${pageContext.request.contextPath}/resources/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.14/css/mdb.min.css" rel="stylesheet">

    <!-- Bootstrap tooltips -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"
            type="text/javascript"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"
            type="text/javascript"></script>
    <!-- MDB core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.14/js/mdb.min.js"
            type="text/javascript"></script>
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<div class="d-flex justify-content-center align-items-center">
    <form action="authentication" class="text-center border border-light p-5 my-5 col-4 section" method="post">
        <p class="h4 mb-4">Вход</p>
        <c:if test="${param.error != null}">
            <p class="errorMessage">Пользователь с таким логином и паролем не найден!</p>
        </c:if>
        <input class="form-control mb-4" id="defaultLoginFormEmail" name="email" placeholder="E-mail" type="email">
        <input class="form-control mb-4" id="defaultLoginFormPassword" name="password" placeholder="Password"
               type="password">
        <div class="d-flex justify-content-center">
            <div class="custom-control custom-checkbox">
                <input class="custom-control-input" id="defaultLoginFormRemember" name="remember-me" type="checkbox"
                       value="true">
                <label class="custom-control-label" for="defaultLoginFormRemember">Remember me</label>
            </div>
        </div>
        <button class="btn btn-info btn-block my-4" type="submit">Войти</button>
        <p>Еще не регистрировались ?
            <a href="register">Зарегистрироваться!</a>
        </p>
    </form>
</div>
</body>
</html>
