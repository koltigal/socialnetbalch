<%@page import="java.util.Date"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@page contentType="text/html; charset=UTF-8" %>
<html>
<meta charset="UTF-8">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">

    <!-- My own CSS-->
    <link href="${pageContext.request.contextPath}/resources/css/userPageStyle.css" rel="stylesheet" type="text/css"/>
    <!-- JQuery UI CSS-->
    <link href="${pageContext.request.contextPath}/resources/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.14/css/mdb.min.css" rel="stylesheet">

    <!-- Bootstrap tooltips -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"
            type="text/javascript"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"
            type="text/javascript"></script>
    <!-- MDB core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.14/js/mdb.min.js"
            type="text/javascript"></script>
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- JQuery UI -->
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui.min.js"></script>

    <script src="${pageContext.request.contextPath}/resources/js/ui.js"></script>
</head>
<body>

<div class="d-flex justify-content-center">
    <form action="register" class="text-center border border-light p-5 my-5 col-4 section" enctype="multipart/form-data"
          method="post">
        <p class="h4 mb-4">Регистрация</p>
        <small class="form-text text-muted mb-1" id="registerFormHelpBlock">
            Все поля отмеченные звездочкой являются обязательными
        </small>
        <input name="id" type="hidden" value="-1"/>
        <input class="form-control mb-2" id="registerFormEmail" name="email" placeholder="E-mail*" required
               type="email">
        <input class="form-control mb-2" id="registerFormPassword" name="password" placeholder="Пароль*"
               required type="password">
        <input class="form-control mb-2" id="registerFormName" name="name" placeholder="ФИО*" required>
        <div class="input-group mb-2">
            <div class="custom-file">
                <input aria-describedby="inputGroupFileAddon01" class="custom-file-input" id="inputGroupFile01"
                       name="avatar" placeholder="Аватар"
                       type="file">
                <label class="custom-file-label text-truncate text-muted text-left" for="inputGroupFile01"
                       style="font-family: sans-serif">Аватар</label>
            </div>
        </div>
        <input class="form-control mb-2" id="datePicker" name="birthDate" placeholder="Дата рождения" type="text">
        <input class="form-control mb-2" id="registerFormSkype" name="skype" placeholder="Skype">
        <input class="form-control mb-2" id="registerFormIcq" name="icq" placeholder="ICQ" type="number">
        <input class="form-control mb-2" id="registerFormPhone" name="phone" placeholder="Телефон" type="tel">
        <button class="btn btn-info btn-block my-4" type="submit">Зарегистрироваться!</button>
    </form>
</div>
</body>
</html>
