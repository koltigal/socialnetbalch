<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<div class="d-flex justify-content-center">
    <div class="col-9 section text-center">
        <p class="h4 mb-2">Запросы на добавление в управляемые вами группы</p>
        <table class="col-12">
            <c:choose>
                <c:when test="${not empty communityRequestsToAccount}">
                    <c:forEach items="${communityRequestsToAccount}" var="communityRequestToAccount">
                        <tr>
                            <td>
                                <div class="d-flex border-top border-light justify-content-center">
                                    <div class="mb-2 col-4 text-center align-self-center">
                                        <a href="user-page?pageUserId=${communityRequestToAccount.requestingUser.id}">
                                            <img class="img-fluid rounded-circle hoverable"
                                                 height="50"
                                                 onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/default-avatar-250x250.png';"
                                                 src="images?imageType=personAvatar&imageId=${communityRequestToAccount.requestingUser.id}"
                                                 width="50">
                                        </a>
                                        <b>
                                            <a href="user-page?pageUserId=${communityRequestToAccount.requestingUser.id}">${communityRequestToAccount.requestingUser.name}</a>
                                        </b>
                                    </div>
                                    <div class="mb-2 col-3 text-center align-self-center">
                                        <a href="community?communityId${communityRequestToAccount.requestedCommunity.id}">
                                            <img class="img-fluid rounded-circle hoverable"
                                                 height="50"
                                                 onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/groupDefault.jpg';"
                                                 src="images?imageType=groupAvatar&imageId=${communityRequestToAccount.requestedCommunity.id}"
                                                 width="50">
                                        </a>
                                        <b>
                                            <a href="community?communityId=${communityRequestToAccount.requestedCommunity.id}">${communityRequestToAccount.requestedCommunity.communityName}</a>
                                        </b>
                                    </div>
                                    <div class="col-5 d-flex justify-content-around">
                                        <br/>
                                        <form action="community-request-handler" method="post">
                                            <input name="requestingUserId" type="hidden"
                                                   value="${communityRequestToAccount.requestingUser.id}"/>
                                            <input name="communityId" type="hidden"
                                                   value="${communityRequestToAccount.requestedCommunity.id}"/>
                                            <input class="btn btn-info btm-sm my-1" name="approve" type="submit"
                                                   value="Подтвердить"/>
                                        </form>
                                        <form action="community-request-handler" method="post">
                                            <input name="requestingUserId" type="hidden"
                                                   value="${communityRequestToAccount.requestingUser.id}"/>
                                            <input name="communityId" type="hidden"
                                                   value="${communityRequestToAccount.requestedCommunity.id}"/>
                                            <input class="btn btn-info btm-sm my-1" name="disapprove" type="submit"
                                                   value="Отклонить"/>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <tr>
                        <td>
                            <p class="border-top border-light text-center">Нет запросов</p>
                        </td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </table>
    </div>
</div>

<div class="d-flex justify-content-center">
    <div class="col-9 section text-center">
        <p class="h4 mb-2">Ваши запросы на добавление в группу</p>
        <table class="col-12">
            <c:choose>
                <c:when test="${not empty communityRequestsFromAccount}">
                    <c:forEach items="${communityRequestsFromAccount}" var="communityRequestFromAccount">
                        <tr>
                            <td>
                                <div class="d-flex border-top border-light justify-content-center">
                                    <div class="mb-2 col-4 text-center align-self-center">
                                        <a href="community?communityId${communityRequestFromAccount.requestedCommunity.id}">
                                            <img class="img-fluid rounded-circle hoverable"
                                                 height="50"
                                                 onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/groupDefault.jpg';"
                                                 src="images?imageType=groupAvatar&imageId=${communityRequestFromAccount.requestedCommunity.id}"
                                                 width="50">
                                        </a>
                                        <b>
                                            <a href="community?communityId=${communityRequestFromAccount.requestedCommunity.id}">${communityRequestFromAccount.requestedCommunity.communityName}</a>

                                        </b>
                                    </div>
                                    <div class="col-5 d-flex justify-content-center">
                                        <form action="community-request-handler" method="post">
                                            <input name="requestingUserId" type="hidden"
                                                   value="${communityRequestFromAccount.requestingUser.id}"/>
                                            <input name="communityId" type="hidden"
                                                   value="${communityRequestFromAccount.requestedCommunity.id}"/>
                                            <input class="btn btn-info btm-sm my-1" name="disapprove" type="submit"
                                                   value="Отозвать заявку"/>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <tr>
                        <td><p class="border-top border-light text-center">Нет запросов</p></td>
                    </tr>
                </c:otherwise>
            </c:choose>
        </table>
    </div>
</div>
</body>
</html>