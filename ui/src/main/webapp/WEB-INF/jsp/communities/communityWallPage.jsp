<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<body>

<div class="section align-self-start">
    <form action="community-wall" enctype="multipart/form-data" method="post">
        <div class="form-group shadow-textarea">
            <textarea class="form-control z-depth-1" name="wallMessageText" placeholder="Напишите сообщение.."
                      rows="3"></textarea>
        </div>
        <div class="d-flex justify-content-around">
            <div class="input-group m-2">
                <div class="custom-file">
                    <input aria-describedby="inputGroupFileAddon02" class="custom-file-input" id="inputGroupFile02"
                           name="photo" placeholder="Аватар"
                           type="file">
                    <label class="custom-file-label text-truncate text-muted text-left" for="inputGroupFile02"
                           style="font-family: sans-serif">Выберите фото</label>
                </div>
            </div>
            <div>
                <input class="btn btn-info btn-sm m-2" type="submit" value="Отправить"/>
            </div>
        </div>
    </form>
</div>

<table class="col-12">
    <c:forEach items="${wallChat}" var="message">
        <tr>
            <td>
                <div class="section">
                    <div class="d-flex  mb-4">
                        <div class="col-2">
                            <a href="user-page?pageUserId=${message.sender.id}">
                                <img class="img-fluid rounded-circle hoverable"
                                     height="50"
                                     onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/default-avatar-250x250.png';"
                                     src="images?imageType=personAvatar&imageId=${message.sender.id}" width="50">
                            </a>
                        </div>
                        <div class="col-10 text-left align-self-center">
                            <b>
                                <a href="user-page?pageUserId=${message.sender.id}">${message.sender.name}</a>
                            </b><br>
                            <small>
                                <c:out value="${message.date}"></c:out>
                            </small>
                        </div>
                    </div>
                    <div class="col-12 text-left ">
                        <c:out value="${message.text}"></c:out>
                        </br>
                        <c:choose>
                            <c:when test="${message.photoExists}">
                                <a href="images?imageType=communityMessageImage&imageId=${message.id}">
                                    <img height="50" src="images?imageType=communityMessageImage&imageId=${message.id}"
                                         width="50">
                                </a>
                            </c:when>
                        </c:choose>
                    </div>
                </div>
            </td>
        </tr>
    </c:forEach>
</table>

</body>
</html>