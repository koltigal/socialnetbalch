<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<div class="d-flex justify-content-center">
    <form action="create-community"
          class="text-center border border-light p-5 my-5 col-5 section" enctype="multipart/form-data" method="post">
        <p class="h4 mb-4">Создание сообщества</p>
        <small class="form-text text-muted mb-1" id="settingsFormHelpBlock">
            Поля отмеченные звездочкой нельзя оставлять пустыми
        </small>

        <div class="d-flex mb-2">
            <div class="col-3 text-right align-self-center">
                <p class="text-muted">Название*: </p>
            </div>
            <div class="col-9 align-self-center">
                <input class="form-control mb-2" name="communityName" placeholder="Название" required/>
            </div>
        </div>

        <div class="d-flex mb-2">
            <div class="col-3 text-right align-self-center">
                <p class="text-muted">Описание*: </p>
            </div>
            <div class="col-9">
                <input class="form-control mb-2" name="description" placeholder="Описание" required/>
            </div>
        </div>

        <div class="d-flex mb-2">
            <div class="col-3 text-right align-self-center">
                <p class="text-muted">Аватар: </p>
            </div>
            <div class="input-group col-9">
                <div class="custom-file">
                    <input aria-describedby="inputGroupFileAddon03" class="custom-file-input" id="inputGroupFile03"
                           name="avatar"
                           placeholder="Аватар"
                           type="file">
                    <label class="custom-file-label text-truncate text-muted text-left" for="inputGroupFile03"
                           style="font-family: sans-serif">Аватар</label>
                </div>
            </div>
        </div>

        <input class="btn btn-info btn-block my-1" id="saveUserSettings" type="submit" value="Создать сообщество"/>
    </form>
</div>

</body>
</html>