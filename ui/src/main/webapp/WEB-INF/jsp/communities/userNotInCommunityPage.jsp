<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<div class="d-flex flex-column section col-6 mx-auto">
    <p class="h4 mb-4">Вы не состоите в сообществе ${community.communityName}, но вы можете подать заявку!</p>
    <form action="community">
        <input name="requestedCommunityId" type="hidden" value="${community.id}">
        <input class="btn btn-info btn-block my-4" type="submit" value="Подать заявку">
    </form>
</div>
</body>
</html>