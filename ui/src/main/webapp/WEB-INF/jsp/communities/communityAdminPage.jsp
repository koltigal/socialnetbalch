<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<div class="d-flex flex-column section text-center col-4 mx-auto">
    <p class="h4 mb-2">Администрирование сообщества</p>
    <form action="community-administration-page?communityId=${community.id}" class="text-center"
          enctype="multipart/form-data" method="post">
        <div class="input-group">
            <div class="custom-file">
                <input class="custom-file-input" name="changeAvatar" type="hidden"/>
                <input aria-describedby="inputGroupFileAddon03" class="custom-file-input" id="inputGroupFile03"
                       name="avatar"
                       placeholder="Аватар"
                       type="file">
                <label class="custom-file-label text-truncate text-muted text-left" for="inputGroupFile03"
                       style="font-family: sans-serif">Аватар</label>
            </div>
        </div>
        <input class="btn btn-info btn-block my-1" type="submit" value="Сменить аватар"/>
    </form>

    <form action="community-administration-page?communityId=${community.id}" class="text-center border-top border-light"
          enctype="multipart/form-data" method="post">
        <input name="deleteCommunity" type="hidden" value="true"/>
        <input class="btn btn-info btn-block my-1" type="submit" value="Удалить сообщество"/>
    </form>
</div>

<div class="d-flex text-center flex-column section col-4 mx-auto">
    <p class="h4 mb-2">Список администраторов сообщества</p>
    <table>
        <c:choose>
            <c:when test="${not empty communityAdmins}">
                <c:forEach items="${communityAdmins}" var="admin">
                    <tr>
                        <td>
                            <div class="d-flex mb-2 border-top border-light">
                                <div class="col-2 text-left">
                                    <a href="user-page?pageUserId=${admin.id}">
                                        <img class="img-fluid rounded-circle hoverable"
                                             height="50"
                                             onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/default-avatar-250x250.png';"
                                             src="images?imageType=personAvatar&imageId=${admin.id}" width="50">
                                    </a>
                                </div>
                                <div class="col-10 text-left align-self-center">
                                    <b>
                                        <a href="user-page?pageUserId=${admin.id}">${admin.name}</a>
                                    </b>
                                </div>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <tr>
                    <td><p>В группе нет ни одного администратора</p></td>
                </tr>
            </c:otherwise>
        </c:choose>
    </table>
</div>

<div class="d-flex text-center flex-column section col-8 mx-auto">
    <p class="h4 mb-2">Список пользователей сообщества</p>
    <table>
        <c:choose>
            <c:when test="${not empty communityUsers}">
                <c:forEach items="${communityUsers}" var="user">
                    <tr>
                        <td>
                            <div class="d-flex mb-2 border-top border-light justify-content-center">
                                <div class="mb-2 col-5 text-left align-self-center">
                                    <a href="user-page?pageUserId=${user.id}">
                                        <img class="img-fluid rounded-circle hoverable"
                                             height="50"
                                             onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/default-avatar-250x250.png';"
                                             src="images?imageType=personAvatar&imageId=${user.id}" width="50">
                                    </a>
                                    <b>
                                        <a href="user-page?pageUserId=${user.id}">${user.name}</a>
                                    </b>
                                </div>
                                <div class="col-7 d-flex justify-content-around">
                                    <form action="community-administration-page?communityId=${community.id}"
                                          enctype="multipart/form-data"
                                          method="post">
                                        <input name="makeCommunityAdminUserId" type="hidden"
                                               value="${user.id}"/>
                                        <input name="makeCommunityAdminCommunityId" type="hidden"
                                               value="${community.id}"/>
                                        <input class="btn btn-info btm-sm my-1" type="submit"
                                               value="Сделать администратором"/>
                                    </form>
                                    <form action="community-administration-page?communityId=${community.id}"
                                          enctype="multipart/form-data"
                                          method="post">
                                        <input name="deleteFromCommunityUserId" type="hidden"
                                               value="${user.id}"/>
                                        <input name="deleteFromCommunityCommunityId" type="hidden"
                                               value="${community.id}"/>
                                        <input class="btn btn-info btm-sm my-1" type="submit"
                                               value="Удалить из группы"/>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <tr>
                    <td class="text-center"><p>Пока нет ни одного пользователя</p></td>
                </tr>
            </c:otherwise>
        </c:choose>
    </table>
</div>

</body>
</html>