<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<div class="d-flex justify-content-center">
    <div class="leftColumn col-3">
        <div class="section align-self-start">
            <img class="d-block mx-auto img-fluid"
                 onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/groupDefault.jpg';"
                 src="images?imageType=groupAvatar&imageId=${community.id}">
            <c:choose>
                <c:when test="${not empty community.description}">
                    <c:out value="${community.description}"/>
                </c:when>
            </c:choose>
        </div>
    </div>

    <div class="rightColumn col-6">
        <div class="section align-self-start text-center">
            <h4 class="my-2">
                <c:out value="${community.communityName}"/>
            </h4>
            <div class="d-flex justify-content-around">
                <c:choose>
                    <c:when test="${adminAttribute eq true}">
                        <div>
                            <a class="btn btn-info btn-block mb-2"
                               href="community-administration-page?communityId=${community.id}"
                               role="button">Администрирование</a>
                        </div>
                    </c:when>
                </c:choose>
                <form action="community?communityId=${community.id}"
                      method="post">
                    <input name="leaveCommunity" type="hidden" value="true"/>
                    <input class="btn btn-info btn-block mb-2" type="submit" value="Выйти из сообщества"/>
                </form>
            </div>
        </div>

        <jsp:include page="/community-wall"/>

    </div>
</div>
</body>
</html>