<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<meta charset="UTF-8">
<body>
<jsp:include page="/WEB-INF/jsp/common/pageHeader.jsp"/>

<div class="d-flex flex-column mb-4 col-4 text-center section mx-auto">
    <p class="h4 mb-2">Список групп</p>
    <table>
        <c:choose>
            <c:when test="${not empty groupsList}">
                <c:forEach items="${groupsList}" var="community">
                    <tr>
                        <td>
                            <div class="d-flex mb-2 border-top border-light">
                                <div class="col-2 ">
                                    <a href="community?communityId${community.id}">
                                        <img class="img-fluid rounded-circle hoverable"
                                             height="50"
                                             onerror="this.onerror=null;this.src='${pageContext.request.contextPath}/resources/css/images/groupDefault.jpg';"
                                             src="images?imageType=groupAvatar&imageId=${community.id}" width="50">
                                    </a>
                                </div>
                                <div class="col-10 text-left align-self-center">
                                    <b>
                                        <a href="community?communityId=${community.id}">${community.communityName}</a>
                                    </b>
                                </div>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <tr>
                    <td><p>Вы не состоите ни в одной группе</p></td>
                </tr>
            </c:otherwise>
        </c:choose>
    </table>
</div>

<form action="create-community" class="text-center  my-2 col-4 section mx-auto">
    <p class="h4 mb-2">Создать сообщество</p>
    <input class="btn btn-info btn-block my-1" type="submit" value="Создать группу"/>
</form>

</body>
</html>