$(document).ready(function() {
    var searchWord = $("#searchWord").val();

    var foundAccountsTable = $('#foundAccountsTable').DataTable({
        "pageLength": 2,
        "ajax": {
            "url": "search?accountSearch=true&ajaxSearchWord=" + searchWord,
            "dataSrc": ''
        },
        "columns": [{
                "data": "id",
                "render": function(data, type, row) {
                    return '<a href="user-page?pageUserId=' + data + '">' +
                        '<img class="img-fluid rounded-circle hoverable"' +
                        'height="50"' +
                        'onerror="this.onerror=null;this.src=\'resources/css/images/default-avatar-250x250.png\';"' +
                        'src="images?imageType=personAvatar&imageId=' + data + '" width="50">' +
                        '</a>'
                }
            },
            {
                "data": null,
                "render": function(data, type, row) {
                    return '<a href="user-page?pageUserId=' + data.id + '">' + data.name + '</a>'
                }
            }
        ]
    });

    var foundCommunitiesTable = $('#foundCommunitiesTable').DataTable({
        "pageLength": 2,
        "ajax": {
            "url": "search?communitySearch=true&ajaxSearchWord=" + searchWord,
            "dataSrc": ''
        },
        "columns": [{
                "data": "id",
                "render": function(data, type, row) {
                    return '<a href="community?communityId=' + data + '">' +
                        '<img class="img-fluid rounded-circle hoverable"' +
                        'height="50"' +
                        'onerror="this.onerror=null;this.src=\'resources/css/images/groupDefault.jpg\';"' +
                        'src="images?imageType=groupAvatar&imageId=' + data + '" width="50">' +
                        '</a>'
                }
            },
            {
                "data": null,
                "render": function(data, type, row) {
                    return '<a href="community?communityId=' + data.id + '">' + data.communityName + '</a>'
                }
            }
        ]
    });

    $(document).on("click", "#searchButton", function(event) {
        event.preventDefault();
        foundAccountsTable.destroy();
        foundCommunitiesTable.destroy();
        var searchWord = $("#searchWord").val();


        $('#foundAccountsTable').DataTable({
            "pageLength": 2,
            "ajax": {
                "url": "search?accountSearch=true&ajaxSearchWord=" + searchWord,
                "dataSrc": ''
            },
            "columns": [{
                    "data": "id",
                    "render": function(data, type, row) {
                        return '<a href="user-page?pageUserId=' + data + '">' +
                            '<img class="img-fluid rounded-circle hoverable"' +
                            'height="50"' +
                            'onerror="this.onerror=null;this.src=\'resources/css/images/default-avatar-250x250.png\';"' +
                            'src="images?imageType=personAvatar&imageId=' + data + '" width="50">' +
                            '</a>'
                    }
                },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        return '<a href="user-page?pageUserId=' + data.id + '">' + data.name + '</a>'
                    }
                }
            ]
        });

        $('#foundCommunitiesTable').DataTable({
            "pageLength": 2,
            "ajax": {
                "url": "search?communitySearch=true&ajaxSearchWord=" + searchWord,
                "dataSrc": ''
            },
            "columns": [{
                    "data": "id",
                    "render": function(data, type, row) {
                        return '<a href="community?communityId=' + data + '">' +
                            '<img class="img-fluid rounded-circle hoverable"' +
                            'height="50"' +
                            'onerror="this.onerror=null;this.src=\'resources/css/images/groupDefault.jpg\';"' +
                            'src="images?imageType=groupAvatar&imageId=' + data + '" width="50">' +
                            '</a>'
                    }
                },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        return '<a href="community?communityId=' + data.id + '">' + data.communityName + '</a>'
                    }
                }
            ]
        });

    });
});