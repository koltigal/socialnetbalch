$(document).ready(function() {
    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });

    $(document).on("click", "#addPhone", function() {
        if (/^\+\d{11}$/.test(($("#newPhone").val()))) {
            $("#noPhonesAdded").remove();
            $("#phoneSection").append('<div class="d-flex mb-2">' +
                '<div class="col-3 text-right align-self-center">' +
                '<p class="text-muted">Телефон: </p>' +
                '</div>' +
                '<div class="input-group col-9 align-self-center">' +
                '<input name="phone" class="form-control" value="' + $("#newPhone").val() + '"/>' +
                '<div class="input-group-append">' +
                '<button type="button" class="deletePhone btn btn-info m-0 px-3 py-2">Удалить</button>' +
                '</div>' +
                '</div>' +
                '</div>');
        } else {
            $("<div>Введенный номер не удовлетворяет формату.\nКорректный формат +X XXX XXX XX XX без пробелов</div>").dialog({
                buttons: [{
                    text: 'OK',
                    click: function() {
                        $(this).dialog('close');
                    }
                }]
            });
        }
    });

    $(document).on("click", ".deletePhone", function() {
        $(this).parent().parent().parent().remove();
    });

    $(document).on("click", "#savePassword", function(event) {
        if ($("#confirmPassword").val() !== $("#password").val()) {
            $("#confirmPassword")[0].setCustomValidity("Пароли не совпадают!");
        } else {
            dialogWindow(event, "Сохранить пароль ?", $(this).parent());
        }
    });

    $(document).on("click", "#saveUserSettings", function(event) {
        dialogWindow(event, "Сохранить изменения ?", $(this).parent());
    });

    $(document).on("click", "#deleteAccount", function(event) {
        dialogWindow(event, "Вы уверены, что хотите удалить аккаунт ?", $(this).parent());
    });

    function dialogWindow(event, text, formElement) {
        event.preventDefault();
        $("<div>" + text + "</div>").dialog({
            buttons: [{
                    text: 'Да',
                    click: function() {
                        $(this).dialog('close');
                        formElement.submit();
                    }
                },
                {
                    text: 'Нет',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
            ]
        });
    }

    $("#datePicker").datepicker({
        dateFormat: "yy-mm-dd"
    });

    $("[title]").tooltip({
        position: {
            my: "left top",
            at: "right+5 top-5",
            collision: "none"
        }
    });

});