$(function() {
    var stompClient = null;

    function setConnected(connected) {
        $("#sendMessage").disabled = !connected;
    }

    function connect() {
        var socket = new SockJS(pageAddress + '/chat');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function(frame) {
            setConnected(true);
            console.log('Connected: ' + frame);
            stompClient.subscribe('/user/topic/messages', function(messageOutput) {
                showMessageOutput(JSON.parse(messageOutput.body));
            });
        });
    }

    connect();

    $(document).on("click", "#sendMessage", function() {
        var text = $("#messageText").val();
        stompClient.send("/app/chat", {}, JSON.stringify({
            'fromId': senderAccountId,
            'fromName': senderAccountName,
            'fromEmail': senderAccountEmail,
            'toEmail': receiverAccountEmail,
            'text': text
        }));
        $("#messageText").val('');
    });

    function showMessageOutput(messageOutput) {
        if ((messageOutput.fromEmail == receiverAccountEmail) || (messageOutput.fromEmail == senderAccountEmail)) {
            $("#chatTable").prepend(
                '<tr>' +
                '<td>' +
                '<div class="section">' +
                '<div class="d-flex  mb-4">' +
                '<div class="col-2">' +
                '<a href="user-page?pageUserId=' + messageOutput.fromId + '">' +
                '<img class="img-fluid rounded-circle hoverable"' +
                'height="50"' +
                'onerror="this.onerror=null;this.src=\'' + pageAddress + '/resources/css/images/default-avatar-250x250.png\';"' +
                'src="images?imageType=personAvatar&imageId=' + messageOutput.fromId + '" width="50">' +
                '</a>' +
                '</div>' +
                '<div class="col-10 text-left align-self-center">' +
                '<b>' +
                '<a href="user-page?pageUserId=' + messageOutput.fromId + '">' + messageOutput.fromName + '</a>' +
                '</b><br>' +
                '<small>' + messageOutput.time +
                '</small>' +
                '</div>' +
                '</div>' +
                '<div class="col-12 text-left ">' + messageOutput.text +
                '</br>' +
                '</div>' +
                '</div>' +
                '</td>' +
                '</tr>'
            );
        }
    }
});