package com.getjavajob.training.webproject1803.balchevskiyn.service;

import com.getjavajob.training.webproject1803.balchevskiyn.dao.MessageDao;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.RootMessage;
import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(rollbackFor = Exception.class)
public class MessageService {

    private MessageDao dao;

    @Autowired
    public MessageService(MessageDao messageDao) {
        this.dao = messageDao;
    }

    @SuppressWarnings("unused")
    RootMessage getMessageById(int messageId, Class messageType) {
        return dao.getMessageById(messageId, messageType);
    }

    public byte[] getMessageImageById(int messageId, Class messageType) {
        return dao.getMessageImageById(messageId, messageType);
    }

    public List<RootMessage> getMessagesByReceiverAndType(int receiverId, Class messageType) {
        return dao.getMessagesByReceiverAndType(receiverId, messageType);
    }

    public List<RootMessage> getAllMessagesBetweenTwoAccountsByType(
            int senderId, int receiverId, Class messageType) {
        return dao.getAllMessagesBetweenTwoAccountsByType(senderId, receiverId, messageType);
    }

    public int addMessageBySenderReceiverAndType(int senderId, int receiverId, String text, Class messageType) {
        return dao.addMessageBySenderReceiverAndType(senderId, receiverId, text, LocalDate.now(),
                messageType);
    }

    public void insertMessageImageById(int messageId, byte[] photo, Class messageType) {
        dao.insertMessageImageById(messageId, photo, messageType);
    }
}
