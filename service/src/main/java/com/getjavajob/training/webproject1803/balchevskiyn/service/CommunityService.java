package com.getjavajob.training.webproject1803.balchevskiyn.service;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.Account.AccountRole;
import com.getjavajob.training.webproject1803.balchevskiyn.Community;
import com.getjavajob.training.webproject1803.balchevskiyn.CommunityUser.CommunityUserRole;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.CommunityDao;
import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(rollbackFor = Exception.class)
public class CommunityService {

    private CommunityDao dao;

    @Autowired
    public CommunityService(CommunityDao communityDao) {
        this.dao = communityDao;
    }

    public List<Community> getCommunitiesListByUserId(int userId) {
        return dao.getCommunitiesListByUserId(userId);
    }

    public List<Community> getCommunitiesListByName(String name) {
        return dao.getCommunitiesListByName(name);
    }

    @SuppressWarnings({"WeakerAccess", "unused"})
    @Transactional(rollbackFor = Exception.class)
    public void insertCommunityUser(int userId, int communityId, AccountRole userRole) {
        dao.insertCommunityUser(userId, communityId);
    }

    public Community getCommunityById(int communityId) {
        return dao.getCommunityById(communityId);
    }

    public byte[] getCommunityAvatarById(int communityId) {
        return dao.getCommunityAvatarById(communityId);
    }

    public boolean checkUserIsAdminInCommunity(int userId, int communityId) {
        return dao.checkUserIsAdminInCommunity(userId, communityId);
    }

    public List<Account> getAllUsersByRoleFromCommunity(int communityId, CommunityUserRole role) {
        return dao.getAllUsersByRoleFromCommunity(communityId, role);
    }

    public boolean checkUserInsideCommunity(int userId, int communityId) {
        return dao.checkUserInsideCommunity(userId, communityId);
    }

    public List<Integer> getIdOfCommunitiesWhereUserIsAdmin(int accountId) {
        return dao.getIdOfCommunitiesWhereUserIsAdmin(accountId);
    }

    public void makeUserCommunityAdmin(int userId, int communityId) {
        dao.makeUserCommunityAdmin(userId, communityId);
    }

    public void deleteCommunityUser(int userId, int communityId) {
        dao.deleteCommunityUser(userId, communityId);
    }

    public int createCommunity(Community community) {
        int communityId =
                dao.createCommunity(community.getCommunityName(), LocalDate.now(),
                        community.getDescription(),
                        community.getCreatorId());
        dao.insertCommunityUser(community.getCreatorId(), communityId);
        dao.makeUserCommunityAdmin(community.getCreatorId(), communityId);
        return communityId;
    }

    public void deleteCommunity(int communityId) {
        dao.deleteCommunity(communityId);
    }

    public void updateAvatarById(int id, byte[] avatar) {
        dao.updateAvatarById(id, avatar);
    }
}
