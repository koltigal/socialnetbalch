package com.getjavajob.training.webproject1803.balchevskiyn.mailing;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.AccountDao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class BirthDayDataProducer {

    private static final Logger logger = LoggerFactory.getLogger(BirthDayDataProducer.class);
    @Autowired
    AccountDao dao;
    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    private Queue queue;

    private List<BirthdayData> getBirthdayData(Date date) {
        List<Account> birthdayAccounts = dao.getBirthdayAccounts(date);
        List<BirthdayData> birthdayDataList = new ArrayList<>();
        for (Account account : birthdayAccounts) {
            List<String> friendsEmail = new ArrayList<>();
            for (int friendId : account.getFriendsIdList()) {
                friendsEmail.add(dao.getAccountById(friendId).getEmail());
            }
            birthdayDataList.add(new BirthdayData(friendsEmail, account.getName()));
        }
        return birthdayDataList;
    }

    @Scheduled(cron = "0 0 8 * * *")
    public void sendBirthdayData() {
        for (BirthdayData birthdayData : getBirthdayData(new Date())) {
            jmsTemplate.send(queue, new MessageCreator() {
                public Message createMessage(Session session) throws JMSException {
                    return session.createObjectMessage(birthdayData);
                }
            });
        }
    }
}

