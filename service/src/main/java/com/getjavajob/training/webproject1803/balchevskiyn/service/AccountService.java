package com.getjavajob.training.webproject1803.balchevskiyn.service;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.Account.AccountRole;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.AccountDao;
import java.sql.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(rollbackFor = Exception.class)
public class AccountService {

    @Autowired
    PasswordEncoder passwordEncoder;
    private AccountDao dao;

    @Autowired
    public AccountService(AccountDao accountDao) {
        this.dao = accountDao;
    }

    public Account getAccountById(int userId) {
        return dao.getAccountById(userId);
    }

    public Account getAccountByEmail(String email) {
        return dao.getAccountByEmail(email);
    }

    @SuppressWarnings("UnusedReturnValue")

    public int createAccount(Account account, byte[] avatar) {
        int newAccountId = dao.insertAccount(account);
        dao.updateAvatarById(newAccountId, avatar);
        return newAccountId;
    }

    public void updatePassword(int accountId, String password) {
        dao.updatePasswordById(accountId, passwordEncoder.encode(password));
    }

    public void updateAccount(
            byte[] avatar, int id, String name, Date birthDate, String email, Integer icq,
            String skype, AccountRole role, List<String> phones) {
        dao.updateAvatarById(id, avatar);
        dao.updateNameById(id, name);
        dao.updateBirthById(id, birthDate);
        dao.updateEmailById(id, email);
        dao.updateIcqById(id, icq);
        dao.updateSkypeById(id, skype);
        dao.updateRoleById(id, role);
        dao.deleteAndInsertPhones(id, phones);
    }

    public boolean checkAccountsAreFriends(int userId, int anotherUserId) {
        return dao.checkAccountsAreFriends(userId, anotherUserId);
    }

    public void deleteAccount(int id) {
        dao.deleteAccountById(id);
    }

    @SuppressWarnings("unused")

    public void addFriend(int accountId, int friendId) {
        dao.insertFriend(accountId, friendId);
    }

    public void deleteFriend(int accountId, int friendId) {
        dao.deleteFriend(accountId, friendId);
    }

    public List<Account> getFriendsList(int accountId) {
        return dao.getFriendsList(accountId);
    }

    public byte[] getAvatarById(int userId) {
        return dao.getAvatarById(userId);
    }

    public List<Account> getAccountsByName(String name) {
        return dao.getAccountsByName(name);
    }

    public String getPasswordByEmail(String email) {
        return dao.getPasswordByEmail(email);
    }

    public int getIdByEmail(String email) {
        return dao.getIdByEmail(email);
    }

    public List<Account> getUsersWithWhichHaveChat(int accountId) {
        return dao.getUsersWithWhichHaveChat(accountId);
    }

}
