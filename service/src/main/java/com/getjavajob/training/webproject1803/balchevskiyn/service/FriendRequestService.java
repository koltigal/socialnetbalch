package com.getjavajob.training.webproject1803.balchevskiyn.service;

import com.getjavajob.training.webproject1803.balchevskiyn.FriendRequest;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.AccountDao;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.FriendRequestDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(rollbackFor = Exception.class)
public class FriendRequestService {

    private FriendRequestDao dao;
    private AccountDao accountDao;

    @Autowired
    public FriendRequestService(FriendRequestDao friendRequestDao, AccountDao accountDao) {
        this.dao = friendRequestDao;
        this.accountDao = accountDao;
    }

    public List<FriendRequest> getFriendRequestsFromAccount(int accountId) {
        return dao.findByRequestingUser_id(accountId);
    }

    public List<FriendRequest> getFriendRequestsToAccount(int accountId) {
        return dao.findByRequestedUser_id(accountId);
    }

    public boolean checkRequestByAccountsId(int userId, int anotherUserId) {
        return dao.existsByRequestingUser_idAndRequestedUser_id(userId, anotherUserId);
    }

    public void deleteFriendRequest(int requestingUserId, int requestedUserId) {
        dao.deleteByRequestingUser_idAndRequestedUser_id(requestingUserId, requestedUserId);
    }

    public void addFriendRequest(int requestingUserId, int requestedUserId) {
        if (accountDao.checkAccountsAreFriends(requestingUserId, requestedUserId)) {
            throw new IllegalArgumentException("Accounts are already friends, cant add friend request");
        }
        dao.save(new FriendRequest(accountDao.getAccountById(requestingUserId),
                accountDao.getAccountById(requestedUserId)));
    }

    public void approveFriendRequest(int requestingUserId, int requestedUserId) {
        dao.deleteByRequestingUser_idAndRequestedUser_id(requestingUserId, requestedUserId);
        accountDao.insertFriend(requestingUserId, requestedUserId);
    }
}
