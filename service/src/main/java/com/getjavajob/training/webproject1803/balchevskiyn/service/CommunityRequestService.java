package com.getjavajob.training.webproject1803.balchevskiyn.service;

import com.getjavajob.training.webproject1803.balchevskiyn.CommunityRequest;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.CommunityDao;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.CommunityRequestDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(rollbackFor = Exception.class)
public class CommunityRequestService {

    private CommunityRequestDao dao;
    private CommunityDao communityDao;

    @Autowired
    public CommunityRequestService(CommunityRequestDao communityRequestDao, CommunityDao communityDao) {
        this.dao = communityRequestDao;
        this.communityDao = communityDao;
    }

    public List<CommunityRequest> getCommunityRequestsFromAccount(int accountId) {
        return dao.getCommunityRequestsByAccountId(accountId);
    }

    public List<CommunityRequest> getRequestsToCommunity(int communityId) {
        return dao.getRequestsToCommunity(communityId);
    }

    public void deleteCommunityRequest(int userId, int communityId) {
        dao.deleteCommunityRequest(userId, communityId);
    }

    public void addCommunityRequest(int userId, int communityId) {
        dao.addCommunityRequest(userId, communityId);
    }

    public void approveCommunityRequest(int userId, int communityId) {
        dao.deleteCommunityRequest(userId, communityId);
        communityDao.insertCommunityUser(userId, communityId);
    }

    public boolean checkRequestByUserAndCommunityId(int userId, int communityId) {
        return dao.checkRequestByUserAndCommunityId(userId, communityId);
    }
}