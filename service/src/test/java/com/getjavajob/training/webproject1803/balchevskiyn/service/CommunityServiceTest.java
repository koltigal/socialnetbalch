package com.getjavajob.training.webproject1803.balchevskiyn.service;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.TestObjectsFactory.getTestCommunity;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.getjavajob.training.webproject1803.balchevskiyn.Community;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.CommunityDao;
import java.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CommunityServiceTest {

    private Community community = getTestCommunity(1);
    @InjectMocks
    private CommunityService communityService;
    @Mock
    private CommunityDao communityDao;

    @Test
    public void createCommunityTest() {
        when(communityDao.createCommunity(community.getCommunityName(), LocalDate.now(),
                community.getDescription(), community.getCreatorId())).thenReturn(1);
        assertEquals(1, communityService.createCommunity(community));
        verify(communityDao, times(1)).insertCommunityUser(community.getCreatorId(), 1);
        verify(communityDao, times(1)).makeUserCommunityAdmin(community.getCreatorId(), 1);
    }
}
