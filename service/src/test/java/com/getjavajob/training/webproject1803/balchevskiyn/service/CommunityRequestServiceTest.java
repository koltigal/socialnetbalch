package com.getjavajob.training.webproject1803.balchevskiyn.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.getjavajob.training.webproject1803.balchevskiyn.dao.CommunityDao;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.CommunityRequestDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CommunityRequestServiceTest {

    @InjectMocks
    private CommunityRequestService communityRequestService;
    @Mock
    private CommunityRequestDao communityRequestDao;
    @Mock
    private CommunityDao communityDao;

    @Test
    public void approveCommunityRequestTest() {
        communityRequestService.approveCommunityRequest(1, 1);
        verify(communityRequestDao, times(1)).deleteCommunityRequest(1, 1);
        verify(communityDao, times(1)).insertCommunityUser(1, 1);
    }
}
