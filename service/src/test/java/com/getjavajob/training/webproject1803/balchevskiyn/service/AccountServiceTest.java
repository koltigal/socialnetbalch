package com.getjavajob.training.webproject1803.balchevskiyn.service;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.TestObjectsFactory.getTestAccount;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.AccountDao;
import java.sql.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    private Account account = getTestAccount(1);
    @InjectMocks
    private AccountService accountService;
    @Mock
    private AccountDao accountDao;

    @Test
    public void createAccountTest() {
        when(accountDao.insertAccount(account)).thenReturn(1);
        byte[] byteAvatar = {17, 17};
        assertEquals(1, accountService.createAccount(account, byteAvatar));
        verify(accountDao, times(1)).insertAccount(account);
        verify(accountDao, times(1)).updateAvatarById(1, byteAvatar);
    }

    @Test
    public void updateAccountTest() {
        byte[] byteAvatar = {17, 17};
        accountService.updateAccount(byteAvatar, account.getId(), account.getName(),
                Date.valueOf(account.getStringBirthDate()), account.getEmail(),
                account.getIcq(), account.getSkype(), account.getRole(), account.getPhones());
        verify(accountDao, times(1)).updateAvatarById(1, byteAvatar);
        verify(accountDao, times(1)).updateBirthById(1, Date.valueOf(account.getStringBirthDate()));
        verify(accountDao, times(1)).updateIcqById(1, account.getIcq());
        verify(accountDao, times(1)).updateSkypeById(1, account.getSkype());
        verify(accountDao, times(1)).updateRoleById(1, account.getRole());
        verify(accountDao, times(1)).deleteAndInsertPhones(1, account.getPhones());
    }
}
