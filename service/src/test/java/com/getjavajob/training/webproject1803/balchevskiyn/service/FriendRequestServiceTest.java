package com.getjavajob.training.webproject1803.balchevskiyn.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.getjavajob.training.webproject1803.balchevskiyn.dao.AccountDao;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.FriendRequestDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FriendRequestServiceTest {

    @InjectMocks
    private FriendRequestService friendRequestService;
    @Mock
    private FriendRequestDao friendRequestDao;
    @Mock
    private AccountDao accountDao;

    @Test(expected = IllegalArgumentException.class)
    public void addFriendRequest() {
        when(accountDao.checkAccountsAreFriends(1, 2)).thenReturn(true);
        friendRequestService.addFriendRequest(1, 2);
    }

    @Test
    public void approveFriendRequest() {
        friendRequestService.approveFriendRequest(1, 2);
        verify(friendRequestDao, times(1)).deleteByRequestingUser_idAndRequestedUser_id(1, 2);
        verify(accountDao, times(1)).insertFriend(1, 2);
    }

}
