package com.getjavajob.training.webproject1803.balchevskiyn.dao;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.Account.AccountRole;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.PersonalMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.PersonalMessage_;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
@SuppressWarnings(value = "unchecked")
public class AccountDao {

    private static final Logger logger = LoggerFactory.getLogger(AccountDao.class);

    private static final String SELECT_ACCOUNT_BY_ID = "from Account where id = :id";
    private static final String SELECT_ACCOUNTS_BY_NAME = "from Account where upper(name) like :name";
    private static final String SELECT_ACCOUNT_BY_EMAIL = "from Account where email = :email";
    private static final String SELECT_ACCOUNT_BY_BIRTHDAY = "FROM Account where DATE_FORMAT(birth, '%m-%d')= :date";

    @PersistenceContext
    private EntityManager entityManager;

    public List<Account> getAccountsByName(String name) {
        return entityManager.createQuery(SELECT_ACCOUNTS_BY_NAME)
                .setParameter("name", "%" + name.toUpperCase() + "%").getResultList();
    }

    public List<Account> getBirthdayAccounts(java.util.Date birthday) {
        Query query = entityManager.createQuery(SELECT_ACCOUNT_BY_BIRTHDAY, Account.class);
        DateFormat dateFormat = new SimpleDateFormat("MM-dd");
        query.setParameter("date", dateFormat.format(birthday));
        return (List<Account>) query.getResultList();
    }

    public byte[] getAvatarById(int userId) {
        return getAccountById(userId).getAvatar();
    }

    public Account getAccountById(int id) {
        return (Account) entityManager.createQuery(SELECT_ACCOUNT_BY_ID)
                .setParameter("id", id).getSingleResult();
    }

    @SuppressWarnings("ConstantConditions")
    public boolean checkAccountsAreFriends(int userId, int anotherUserId) {
        return getAccountById(userId).getFriendsIdList().contains(anotherUserId);
    }

    public int insertAccount(Account account) {
        entityManager.persist(account);
        entityManager.flush();
        return account.getId();
    }

    public void deleteAndInsertPhones(int newUserId, List<String> phones) {
        Account account = getAccountById(newUserId);
        account.setPhones(phones);
    }

    public void insertFriend(int accountId, int friendId) {
        getAccountById(accountId).getFriendsIdList().add(friendId);
        getAccountById(friendId).getFriendsIdList().add(accountId);
    }

    public void deleteFriend(int accountId, int friendId) {
        getAccountById(accountId).getFriendsIdList().remove(Integer.valueOf(friendId));
        getAccountById(friendId).getFriendsIdList().remove(Integer.valueOf(accountId));
    }

    public List<Account> getFriendsList(int accountId) {
        List<Integer> accountFriendsId = getAccountById(accountId).getFriendsIdList();
        List<Account> friends = new ArrayList<>();
        for (int friendId : accountFriendsId) {
            friends.add(getAccountById(friendId));
        }
        return friends;
    }

    public List<Account> getUsersWithWhichHaveChat(int accountId) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PersonalMessage> criteriaQuery = criteriaBuilder.createQuery(PersonalMessage.class);
        Root<PersonalMessage> personalMessageRoot = criteriaQuery.from(PersonalMessage.class);
        criteriaQuery.select(personalMessageRoot);
        criteriaQuery
                .where(criteriaBuilder
                        .equal(personalMessageRoot.get(PersonalMessage_.sender), getAccountById(accountId)));
        List<PersonalMessage> messages = entityManager.createQuery(criteriaQuery).getResultList();
        List<Account> usersWithWhichHaveChat = new ArrayList<>();
        for (PersonalMessage message : messages) {
            Account receiver = message.getReceiver();
            if (!usersWithWhichHaveChat.contains(receiver)) {
                usersWithWhichHaveChat.add(receiver);
            }
        }
        return usersWithWhichHaveChat;
    }

    public Account getAccountByEmail(String email) {
        return (Account) entityManager.createQuery(SELECT_ACCOUNT_BY_EMAIL)
                .setParameter("email", email).getSingleResult();
    }

    public String getPasswordByEmail(String email) {
        return getAccountByEmail(email).getPassword();
    }

    public Integer getIdByEmail(String email) {
        return getAccountByEmail(email).getId();
    }

    public void updatePasswordById(int id, String password) {
        getAccountById(id).setPassword(password);
    }

    public void deleteAccountById(int id) {
        entityManager.remove(getAccountById(id));
    }

    public void updateNameById(int id, String name) {
        getAccountById(id).setName(name);
    }

    public void updateAvatarById(int id, byte[] avatar) {
        getAccountById(id).setAvatar(avatar);
    }

    public void updateBirthById(int id, Date birth) {
        getAccountById(id).setBirthDate(birth);
    }

    public void updateEmailById(int id, String email) {
        getAccountById(id).setEmail(email);
    }

    public void updateIcqById(int id, Integer icq) {
        getAccountById(id).setIcq(icq);
    }

    public void updateSkypeById(int id, String skype) {
        getAccountById(id).setSkype(skype);
    }

    public void updateRoleById(int id, AccountRole role) {
        getAccountById(id).setRole(role);
    }
}
