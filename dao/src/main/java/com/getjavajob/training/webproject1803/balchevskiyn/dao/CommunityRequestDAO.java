package com.getjavajob.training.webproject1803.balchevskiyn.dao;

import com.getjavajob.training.webproject1803.balchevskiyn.CommunityRequest;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unchecked")
@Repository
public class CommunityRequestDao {

    private static final Logger logger = LoggerFactory.getLogger(AccountDao.class);

    private static final String SELECT_COMMUNITY_REQUEST_BY_ACCOUNT_ID_AND_COMMUNITY_ID =
            "from CommunityRequest cr where cr.requestingUser.id = :accountId and"
                    + " cr.requestedCommunity.id = :communityId";
    private static final String SELECT_COMMUNITY_REQUESTS_BY_ACCOUNT_ID =
            "from CommunityRequest cr where cr.requestingUser.id = :accountId";
    private static final String SELECT_COMMUNITY_REQUESTS_BY_COMMUNITY_ID =
            "from CommunityRequest cr where cr.requestedCommunity.id = :communityId";

    private final AccountDao accountDao;
    private final CommunityDao communityDao;
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public CommunityRequestDao(AccountDao accountDao, CommunityDao communityDao) {
        this.accountDao = accountDao;
        this.communityDao = communityDao;
    }

    public CommunityRequest getCommunityRequestByAccountAndCommunityId(int accountId, int communityId) {
        Query query = entityManager.createQuery(SELECT_COMMUNITY_REQUEST_BY_ACCOUNT_ID_AND_COMMUNITY_ID);
        query.setParameter("accountId", accountId);
        query.setParameter("communityId", communityId);
        return (CommunityRequest) query.getSingleResult();
    }

    public List<CommunityRequest> getCommunityRequestsByAccountId(int accountId) {
        return entityManager.createQuery(SELECT_COMMUNITY_REQUESTS_BY_ACCOUNT_ID)
                .setParameter("accountId", accountId).getResultList();
    }

    @SuppressWarnings("ConstantConditions")
    public boolean checkRequestByUserAndCommunityId(int accountId, int communityId) {
        try {
            getCommunityRequestByAccountAndCommunityId(accountId, communityId);
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    public List<CommunityRequest> getRequestsToCommunity(int communityId) {
        return entityManager.createQuery(SELECT_COMMUNITY_REQUESTS_BY_COMMUNITY_ID)
                .setParameter("communityId", communityId).getResultList();
    }

    public void deleteCommunityRequest(int accountId, int communityId) {
        entityManager.remove(getCommunityRequestByAccountAndCommunityId(accountId, communityId));
    }

    public void addCommunityRequest(int accountId, int communityId) {
        CommunityRequest communityRequest =
                new CommunityRequest(
                        accountDao.getAccountById(accountId), communityDao.getCommunityById(communityId), null);
        entityManager.persist(communityRequest);
    }
}
