package com.getjavajob.training.webproject1803.balchevskiyn.dao;

import com.getjavajob.training.webproject1803.balchevskiyn.FriendRequest;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FriendRequestDao extends CrudRepository<FriendRequest, Long> {

    FriendRequest findByRequestingUser_idAndRequestedUser_id(int requestingAccountId, int requestedAccountId);

    List<FriendRequest> findByRequestingUser_id(int accountId);

    List<FriendRequest> findByRequestedUser_id(int accountId);

    boolean existsByRequestingUser_idAndRequestedUser_id(int requestingAccountId, int requestedAccountId);

    long deleteByRequestingUser_idAndRequestedUser_id(int requestingAccountId, int requestedAccountId);
}
