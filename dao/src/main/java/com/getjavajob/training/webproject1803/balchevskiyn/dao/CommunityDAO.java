package com.getjavajob.training.webproject1803.balchevskiyn.dao;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.Community;
import com.getjavajob.training.webproject1803.balchevskiyn.CommunityUser;
import com.getjavajob.training.webproject1803.balchevskiyn.CommunityUser.CommunityUserRole;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unchecked")
@Repository
public class CommunityDao {

    private static final Logger logger = LoggerFactory.getLogger(CommunityDao.class);

    private static final String SELECT_COMMUNITY_BY_ID = "from Community where id = :id";
    private static final String SELECT_COMMUNITIES_BY_NAME = "from Community where upper(communityName) like :name";
    private static final String SELECT_COMMUNITIES_USERS_BY_ACCOUNT_ID =
            "from CommunityUser cu where cu.account.id = :id";
    private static final String SELECT_COMMUNITY_USER_BY_ACCOUNT_ID_AND_COMMUNITY_ID =
            "from CommunityUser cu where cu.account.id = :accountId and cu.community.id = :communityId";
    private static final String SELECT_COMMUNITY_USERS_BY_ACCOUNT_WHERE_ROLE_IS_ADMIN =
            "from CommunityUser cu where cu.account.id = :id and cu.communityUserRole = :role";

    private AccountDao accountDao;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public CommunityDao(AccountDao accountDao) {
        logger.debug("Dao CREATED - CommunityDao");
        this.accountDao = accountDao;
    }

    public Community getCommunityById(int id) {
        return (Community) entityManager.createQuery(SELECT_COMMUNITY_BY_ID)
                .setParameter("id", id).getSingleResult();
    }

    public List<Account> getAllUsersByRoleFromCommunity(int communityId, CommunityUserRole role) {
        List<CommunityUser> communityUsers = getCommunityById(communityId).getUsers();
        List<Account> accounts = new ArrayList<>();
        for (CommunityUser communityUser : communityUsers) {
            if (communityUser.getCommunityUserRole().equals(role)) {
                accounts.add(communityUser.getAccount());
            }
        }
        return accounts;
    }

    public boolean checkUserInsideCommunity(int accountId, int communityId) {
        List<CommunityUser> communityUsers = getCommunityById(communityId).getUsers();
        for (CommunityUser communityUser : communityUsers) {
            if (communityUser.getAccount().getId() == accountId) {
                return true;
            }
        }
        return false;
    }

    public List<Integer> getIdOfCommunitiesWhereUserIsAdmin(int accountId) {
        Query query = entityManager.createQuery(SELECT_COMMUNITY_USERS_BY_ACCOUNT_WHERE_ROLE_IS_ADMIN);
        query.setParameter("id", accountId);
        query.setParameter("role", CommunityUserRole.ADMIN);

        List<CommunityUser> communityUsersWithThisAccountId = query.getResultList();
        List<Integer> idOfCommunitiesWhereUserIsAdmin = new ArrayList<>();
        for (CommunityUser communityUser : communityUsersWithThisAccountId) {
            if (communityUser.getCommunityUserRole().equals(CommunityUserRole.ADMIN)) {
                idOfCommunitiesWhereUserIsAdmin.add(communityUser.getCommunity().getId());
            }
        }
        return idOfCommunitiesWhereUserIsAdmin;
    }

    public byte[] getCommunityAvatarById(int id) {
        return getCommunityById(id).getCommunityAvatar();
    }

    public List<Community> getCommunitiesListByName(String name) {
        return entityManager.createQuery(SELECT_COMMUNITIES_BY_NAME)
                .setParameter("name", "%" + name.toUpperCase() + "%").getResultList();
    }

    public List<Community> getCommunitiesListByUserId(int userId) {
        List<CommunityUser> communityUsersWithThisAccountId =
                entityManager.createQuery(SELECT_COMMUNITIES_USERS_BY_ACCOUNT_ID)
                        .setParameter("id", userId).getResultList();

        List<Community> communities = new ArrayList<>();
        for (CommunityUser communityUser : communityUsersWithThisAccountId) {
            communities.add(communityUser.getCommunity());
        }
        return communities;
    }

    public CommunityUser getCommunityUserByAccountAndCommunity(int accountId, int communityId) {
        Query query = entityManager.createQuery(SELECT_COMMUNITY_USER_BY_ACCOUNT_ID_AND_COMMUNITY_ID);
        query.setParameter("accountId", accountId);
        query.setParameter("communityId", communityId);
        return (CommunityUser) query.getSingleResult();
    }

    @SuppressWarnings("ConstantConditions")
    public boolean checkUserIsAdminInCommunity(int userId, int communityId) {
        return getCommunityUserByAccountAndCommunity(userId, communityId).getCommunityUserRole()
                == CommunityUserRole.ADMIN;
    }

    public int createCommunity(String communityName, LocalDate createDate, String communityDescription, int creatorId) {
        Community community = new Community(communityName, false, createDate, communityDescription, creatorId);
        entityManager.persist(community);
        entityManager.flush();
        return community.getId();
    }

    public void deleteCommunity(int communityId) {
        entityManager.remove(getCommunityById(communityId));
    }

    public void insertCommunityUser(int userId, int communityId) {
        CommunityUser communityUser = new CommunityUser(accountDao.getAccountById(userId),
                getCommunityById(communityId), CommunityUserRole.USER);
        entityManager.persist(communityUser);
    }

    public void makeUserCommunityAdmin(int userId, int communityId) {
        getCommunityUserByAccountAndCommunity(userId, communityId).setCommunityUserRole(CommunityUserRole.ADMIN);
    }

    public void deleteCommunityUser(int userId, int communityId) {
        entityManager.remove(getCommunityUserByAccountAndCommunity(userId, communityId));
    }

    @SuppressWarnings("SameParameterValue")
    public void updateCommunityName(int communityId, String newCommName) {
        Community community = getCommunityById(communityId);
        community.setCommunityName(newCommName);
    }

    public void updateAvatarById(int id, byte[] avatar) {
        getCommunityById(id).setCommunityAvatar(avatar);
    }
}
