package com.getjavajob.training.webproject1803.balchevskiyn.dao;

import com.getjavajob.training.webproject1803.balchevskiyn.messages.CommunityMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.PersonalMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.RootMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.RootMessage_;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.WallMessage;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@SuppressWarnings(value = "unchecked")
public class MessageDao {

    private static final Logger logger = LoggerFactory.getLogger(MessageDao.class);

    private final AccountDao accountDao;
    private final CommunityDao communityDao;
    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    public MessageDao(
            AccountDao accountDao, CommunityDao communityDao) {
        this.accountDao = accountDao;
        this.communityDao = communityDao;
    }

    public RootMessage getMessageById(int messageId, Class messageType) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<RootMessage> criteriaQuery = criteriaBuilder.createQuery(messageType);
        Root<RootMessage> messageRoot = criteriaQuery.from(messageType);
        criteriaQuery.select(messageRoot);
        criteriaQuery.where(criteriaBuilder.equal(messageRoot.get(RootMessage_.ID), messageId));
        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }

    public byte[] getMessageImageById(int id, Class messageType) {
        return getMessageById(id, messageType).getPhoto();
    }

    public List<RootMessage> getMessagesByReceiverAndType(int receiverId, Class messageType) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<RootMessage> criteriaQuery = criteriaBuilder.createQuery(messageType);
        Root<RootMessage> messageRoot = criteriaQuery.from(messageType);
        criteriaQuery.select(messageRoot).orderBy(criteriaBuilder.desc(messageRoot.get(RootMessage_.ID)));
        if (messageType.equals(CommunityMessage.class)) {
            criteriaQuery.where(
                    criteriaBuilder
                            .equal(messageRoot.get(RootMessage_.RECEIVER), communityDao.getCommunityById(receiverId)));
        } else {
            criteriaQuery.where(
                    criteriaBuilder
                            .equal(messageRoot.get(RootMessage_.RECEIVER), accountDao.getAccountById(receiverId)));
        }
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public List<RootMessage> getAllMessagesBetweenTwoAccountsByType(int senderId, int receiverId, Class messageType) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<RootMessage> criteriaQuery = criteriaBuilder.createQuery(messageType);
        Root<RootMessage> messageRoot = criteriaQuery.from(messageType);
        criteriaQuery.select(messageRoot).orderBy(criteriaBuilder.desc(messageRoot.get(RootMessage_.ID)));
        criteriaQuery.where(
                criteriaBuilder.equal(messageRoot.get(RootMessage_.SENDER), accountDao.getAccountById(senderId)),
                criteriaBuilder.equal(messageRoot.get(RootMessage_.RECEIVER), accountDao.getAccountById(receiverId)));

        criteriaQuery.where(
                criteriaBuilder.or(
                        criteriaBuilder
                                .equal(messageRoot.get(RootMessage_.SENDER), accountDao.getAccountById(senderId)),
                        criteriaBuilder
                                .equal(messageRoot.get(RootMessage_.SENDER), accountDao.getAccountById(receiverId))
                ),
                criteriaBuilder.or(
                        criteriaBuilder
                                .equal(messageRoot.get(RootMessage_.RECEIVER), accountDao.getAccountById(receiverId)),
                        criteriaBuilder
                                .equal(messageRoot.get(RootMessage_.RECEIVER), accountDao.getAccountById(senderId))
                )
        );

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public int addMessageBySenderReceiverAndType(
            int senderId, int receiverId, String text, LocalDate date, Class messageType) {
        RootMessage rootMessage;
        if (messageType.equals(CommunityMessage.class)) {
            rootMessage = new CommunityMessage(null, accountDao.getAccountById(senderId),
                    communityDao.getCommunityById(receiverId), text, false, null, date);
        } else if (messageType.equals(PersonalMessage.class)) {
            rootMessage = new PersonalMessage(null, accountDao.getAccountById(senderId),
                    accountDao.getAccountById(receiverId), text, date);
        } else {
            rootMessage = new WallMessage(null, accountDao.getAccountById(senderId),
                    accountDao.getAccountById(receiverId), text, false, null, date);
        }
        entityManager.persist(rootMessage);
        entityManager.flush();
        return rootMessage.getId();
    }

    public void insertMessageImageById(int messageId, byte[] photo, Class messageType) {
        getMessageById(messageId, messageType).setPhoto(photo);
    }
}
