package com.getjavajob.training.webproject1803.balchevskiyn.utils;

import com.getjavajob.training.webproject1803.balchevskiyn.Account;
import com.getjavajob.training.webproject1803.balchevskiyn.Account.AccountRole;
import com.getjavajob.training.webproject1803.balchevskiyn.Community;
import com.getjavajob.training.webproject1803.balchevskiyn.CommunityUser;
import com.getjavajob.training.webproject1803.balchevskiyn.CommunityUser.CommunityUserRole;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.CommunityMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.PersonalMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.RootMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.WallMessage;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class TestObjectsFactory {

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static Account getTestAccount(int accountId) {
        List<String> phones = new ArrayList<>();
        List<Integer> friends = new ArrayList<>();
        byte[] byteAvatar = {17, 17};
        if (accountId == 1) {
            phones.add("+79037747207");
            phones.add("+79037747208");
            friends.add(2);
            friends.add(3);

            return new Account(1,
                    "Бальчевский Николай Леонидович", Date.valueOf("1992-02-12"),
                    phones, "koltigal@gmail.com", 123456789, "tangoInharlem@skype",
                    "$2a$12$OhjRaJmjeScNGvxzYk.FoeWTThDFRkkOM/7VnWSQOcgQa19QFepR6",
                    AccountRole.valueOf("ADMIN"), true, byteAvatar, Date.valueOf("2018-02-12"), friends);
        } else if (accountId == 2) {
            phones.add("+79022222222");
            phones.add("+79033333333");
            friends.add(1);
            return new Account(2,
                    "Почуева Ольга Сергеевна", Date.valueOf("1991-02-11"),
                    phones, "Pochuichik@gmail.com", 987654321, "Pochuichik@skype",
                    "$2a$12$IMdG1N4455HzWbjyiw9WjuYgugqWR.tqOBbNP2eyjBJT8hCkVhnDy",
                    AccountRole.valueOf("USER"), false, null, Date.valueOf("2018-02-12"), friends);
        } else if (accountId == 3) {
            friends.add(1);
            phones.add("+79000000000");
            phones.add("+79111111111");
            return new Account(3,
                    "Пупкин Василий Васильевич", Date.valueOf("2000-11-21"),
                    phones, "Vasys@mail.com", 444444444, "Vasil@skype",
                    "$2a$12$KZwadESnNaMh9xfUjvLaFumRbpE3jPqVjzH3vDpa7uJRgJhfMdTmK",
                    AccountRole.valueOf("USER"), false, null, Date.valueOf("2018-02-12"), friends);
        } else if (accountId == 4) {
            phones.add("+79037747207");
            phones.add("+79037747208");
            return new Account(4,
                    "Новый Николай Леонидович", Date.valueOf("1992-02-12"),
                    phones, "koltigal@gmail.com", 123456789, "tangoInharlem@skype",
                    "$2a$12$OhjRaJmjeScNGvxzYk.FoeWTThDFRkkOM/7VnWSQOcgQa19QFepR7",
                    AccountRole.valueOf("ADMIN"), true, byteAvatar,
                    Date.valueOf("2018-02-12"), null);
        } else {
            throw new IllegalArgumentException("Only 1, 2, and 3 test accounts");
        }
    }

    public static Community getTestCommunity(int id) {
        List<CommunityUser> communityUsers = new ArrayList<>();
        if (id == 1) {
            Community community = new Community(1, "New community", true, LocalDate.parse("2018-08-01", formatter),
                    "Новая группа",
                    1);
            communityUsers.add(new CommunityUser(getTestAccount(1), community, CommunityUserRole.ADMIN));
            communityUsers.add(new CommunityUser(getTestAccount(2), community, CommunityUserRole.USER));
            community.setUsers(communityUsers);
            byte[] avatarBytes = {17, 17};
            community.setCommunityAvatar(avatarBytes);
            return community;
        } else if (id == 2) {
            Community community = new Community(2, "New community2", false, LocalDate.parse("2018-08-01", formatter),
                    "Новая группа2",
                    3);
            communityUsers.add(new CommunityUser(getTestAccount(3), community, CommunityUserRole.ADMIN));
            community.setUsers(communityUsers);
            return community;
        } else {
            throw new IllegalArgumentException("Community Id should be 1 or 2");
        }
    }

    public static RootMessage getTestMessage(String type) {
        if ("personal".equals(type)) {
            return new PersonalMessage(1, getTestAccount(1), getTestAccount(2), "testPersonal",
                    LocalDate.parse("2018-08-01", formatter));
        } else if ("community".equals(type)) {
            return new CommunityMessage(1, getTestAccount(1), getTestCommunity(1), "testCommunity", false, null,
                    LocalDate.parse("2018-08-01", formatter));
        } else if ("wall".equals(type)) {
            return new WallMessage(1, getTestAccount(3), getTestAccount(1), "testWall", false, null,
                    LocalDate.parse("2018-08-01", formatter));
        } else {
            throw new IllegalArgumentException("Argument should be personal, community or wall");
        }
    }
}
