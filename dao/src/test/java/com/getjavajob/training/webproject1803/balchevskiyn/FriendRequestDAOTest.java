package com.getjavajob.training.webproject1803.balchevskiyn;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.TestObjectsFactory.getTestAccount;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.getjavajob.training.webproject1803.balchevskiyn.dao.FriendRequestDao;
import com.getjavajob.training.webproject1803.balchevskiyn.testConfig.TestApplication;
import com.getjavajob.training.webproject1803.balchevskiyn.utils.PrepareDatabaseRule;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TestApplication.class)
@Transactional
public class FriendRequestDaoTest {

    @Rule
    @Autowired
    public PrepareDatabaseRule prepareDatabaseRule;
    @Autowired
    private FriendRequestDao dao;
    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("SameParameterValue")
    private FriendRequest getTestFriendRequest(int requestingUserId) {
        if (requestingUserId == 3) {
            return new FriendRequest(getTestAccount(3), getTestAccount(1));
        } else {
            throw new IllegalArgumentException("Requesting user id should be 3");
        }
    }

    @Test
    public void getFriendRequestByAccountsIdTest() {
        assertEquals("FriendRequestDaoTest.getFriendRequestsFromAccountTest", getTestFriendRequest(3),
                dao.findByRequestingUser_idAndRequestedUser_id(3, 1)
        );
    }

    @Test
    public void getFriendRequestsFromAccountTest() {
        List<FriendRequest> expectedCommunityRequests = new ArrayList<>();
        expectedCommunityRequests.add(getTestFriendRequest(3));
        assertEquals("FriendRequestDaoTest.getFriendRequestsFromAccountTest", expectedCommunityRequests,
                dao.findByRequestingUser_id(3)
        );
    }

    @Test
    public void getFriendRequestsToAccountTest() {
        List<FriendRequest> expectedCommunityRequests = new ArrayList<>();
        expectedCommunityRequests.add(getTestFriendRequest(3));
        assertEquals("FriendRequestDaoTest.getFriendRequestsToAccountTest", expectedCommunityRequests,
                dao.findByRequestedUser_id(1)
        );
    }

    @Test
    public void checkRequestByAccountsIdTest() {
        assertTrue("FriendRequestDaoTest.checkRequestByAccountsIdTest",
                dao.existsByRequestingUser_idAndRequestedUser_id(3, 1));
        assertFalse("FriendRequestDaoTest.checkRequestByAccountsIdTest",
                dao.existsByRequestingUser_idAndRequestedUser_id(3, 2));
    }

    @Test
    public void deleteFriendRequestTest() {
        dao.deleteByRequestingUser_idAndRequestedUser_id(3, 1);
        assertFalse("FriendRequestDaoTest.deleteFriendRequestTest",
                dao.existsByRequestingUser_idAndRequestedUser_id(3, 1));
    }

    @Test
    public void addFriendRequestTest() {
        dao.save(new FriendRequest(getTestAccount(1), getTestAccount(3)));
        assertTrue("FriendRequestDaoTest.addFriendRequestTest", dao.existsByRequestingUser_idAndRequestedUser_id(1, 3));
    }
}
