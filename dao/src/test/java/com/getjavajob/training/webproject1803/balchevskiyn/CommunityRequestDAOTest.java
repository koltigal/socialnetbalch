package com.getjavajob.training.webproject1803.balchevskiyn;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.TestObjectsFactory.getTestAccount;
import static com.getjavajob.training.webproject1803.balchevskiyn.utils.TestObjectsFactory.getTestCommunity;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.getjavajob.training.webproject1803.balchevskiyn.dao.CommunityRequestDao;
import com.getjavajob.training.webproject1803.balchevskiyn.testConfig.TestApplication;
import com.getjavajob.training.webproject1803.balchevskiyn.utils.PrepareDatabaseRule;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TestApplication.class)
@Transactional
public class CommunityRequestDaoTest {

    @Rule
    @Autowired
    public PrepareDatabaseRule prepareDatabaseRule;
    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private CommunityRequestDao dao;

    private CommunityRequest getTestCommunityRequest(int requestingUserId) {
        if (requestingUserId == 1) {
            return new CommunityRequest(getTestAccount(1), getTestCommunity(2), "test");
        } else if (requestingUserId == 2) {
            return new CommunityRequest(getTestAccount(3), getTestCommunity(1), "test");
        } else {
            throw new IllegalArgumentException("Requesting user id should be 1 or 3");
        }
    }

    @Test
    public void getCommunityRequestByAccountAndCommunityIdTest() {
        CommunityRequest expectedCommunityRequest = new CommunityRequest(getTestAccount(3), getTestCommunity(1),
                "test");
        assertEquals("CommunityRequestDaoTest.getCommunityRequestByAccountAndCommunityIdTest", expectedCommunityRequest,
                dao.getCommunityRequestByAccountAndCommunityId(3, 1));
    }

    @Test
    public void getCommunityRequestsByAccountIdTest() {
        List<CommunityRequest> expectedCommunityRequests = new ArrayList<>();
        expectedCommunityRequests.add(getTestCommunityRequest(1));
        assertEquals("CommunityRequestDaoTest.getCommunityRequestsFromAccountTest", expectedCommunityRequests,
                dao.getCommunityRequestsByAccountId(1));
    }

    @Test
    public void checkRequestByUserAndCommunityIdTest() {
        assertTrue(
                "CommunityRequestDaoTest.checkRequestByUserAndCommunityIdTest",
                dao.checkRequestByUserAndCommunityId(1, 2));
        assertFalse(
                "CommunityRequestDaoTest.checkRequestByUserAndCommunityIdTest",
                dao.checkRequestByUserAndCommunityId(1, 3));
    }

    @Test
    public void getRequestsToCommunityTest() {
        List<CommunityRequest> expectedCommunityRequests = new ArrayList<>();
        expectedCommunityRequests.add(getTestCommunityRequest(1));
        assertEquals("CommunityRequestDaoTest.getRequestsToCommunityTest", expectedCommunityRequests,
                dao.getRequestsToCommunity(2));
    }

    @Test
    public void deleteCommunityRequestTest() {
        dao.deleteCommunityRequest(1, 2);
        assertEquals("CommunityRequestDaoTest.deleteCommunityRequestTest", new ArrayList<>(),
                dao.getRequestsToCommunity(2));
    }

    @Test
    public void addCommunityRequestTest() {
        dao.addCommunityRequest(2, 2);
        entityManager.flush();
        List<CommunityRequest> expectedCommunityRequests = new ArrayList<>();
        expectedCommunityRequests.add(getTestCommunityRequest(1));
        expectedCommunityRequests.add(new CommunityRequest(getTestAccount(2), getTestCommunity(2), null));
        assertEquals("CommunityRequestDaoTest.addCommunityRequestTest", expectedCommunityRequests,
                dao.getRequestsToCommunity(2));
    }
}

