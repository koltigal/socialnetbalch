package com.getjavajob.training.webproject1803.balchevskiyn;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.TestObjectsFactory.getTestAccount;
import static com.getjavajob.training.webproject1803.balchevskiyn.utils.TestObjectsFactory.getTestCommunity;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.getjavajob.training.webproject1803.balchevskiyn.CommunityUser.CommunityUserRole;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.AccountDao;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.CommunityDao;
import com.getjavajob.training.webproject1803.balchevskiyn.testConfig.TestApplication;
import com.getjavajob.training.webproject1803.balchevskiyn.utils.PrepareDatabaseRule;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TestApplication.class)
@Transactional
public class CommunityDaoTest {

    @Rule
    @Autowired
    public PrepareDatabaseRule prepareDatabaseRule;
    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private AccountDao accountDao;
    @Autowired
    private CommunityDao dao;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Test
    public void getCommunityByIdTest() {
        Community actual = dao.getCommunityById(1);
        assertEquals("CommunityDaoTest.getCommunityByIdTest", getTestCommunity(1), actual);
    }

    @Test
    public void getCommunityAvatarByIdTest() {
        byte[] expectedByteAvatar = {17, 17};
        assertArrayEquals("CommunityDaoTest.getCommunityByIdTest", expectedByteAvatar, dao.getCommunityAvatarById(1));
    }

    @Test
    public void getCommunitiesListByNameTest() {
        List<Community> expected = new ArrayList<>();
        expected.add(getTestCommunity(1));
        expected.add(getTestCommunity(2));
        assertEquals("CommunityDaoTest.getCommunitiesListByNameTest", expected, dao.getCommunitiesListByName("new"));
    }

    @Test
    public void getCommunitiesListByUserIdTest() {
        List<Community> expected = new ArrayList<>();
        expected.add(getTestCommunity(2));
        assertEquals("CommunityDaoTest.getCommunitiesListByNameTest", expected, dao.getCommunitiesListByUserId(3));
    }

    @Test
    public void checkUserIsAdminInCommunityTest() {
        assertTrue("CommunityDaoTest.checkUserIsAdminInCommunityTest", dao.checkUserIsAdminInCommunity(1, 1));
        assertFalse("CommunityDaoTest.checkUserIsAdminInCommunityTest2", dao.checkUserIsAdminInCommunity(2, 1));
    }

    @Test
    public void insertCommunityTest() {
        Community expectedCommunity = new Community(3, "New community2", false,
                LocalDate.parse("2018-08-01", formatter), "Тестовая группа", 3);
        int communityId = dao
                .createCommunity("New community2", LocalDate.parse("2018-08-01", formatter), "Тестовая группа", 3);
        assertEquals("CommunityDaoTest.insertCommunityTest1", 3, communityId);
        entityManager.flush();
        assertEquals("CommunityDaoTest.insertCommunityTest2", expectedCommunity, dao.getCommunityById(3));
    }

    @Test(expected = java.lang.Exception.class)
    public void deleteCommunityTest() {
        dao.deleteCommunity(1);
        entityManager.flush();
        assertNull("CommunityDaoTest.deleteCommunityTest", dao.getCommunityById(1));
    }

    @Test
    public void insertCommunityUserTest() {
        List<CommunityUser> communityUsers = new ArrayList<>();
        Community expectedCommunity = getTestCommunity(1);
        communityUsers.add(new CommunityUser(getTestAccount(1), expectedCommunity, CommunityUserRole.ADMIN));
        communityUsers.add(new CommunityUser(getTestAccount(2), expectedCommunity, CommunityUserRole.USER));
        communityUsers.add(new CommunityUser(getTestAccount(3), expectedCommunity, CommunityUserRole.USER));
        expectedCommunity.setUsers(communityUsers);
        dao.insertCommunityUser(3, 1);
        entityManager.flush();
        assertEquals(
                "CommunityDaoTest.insertCommunityUserTest", expectedCommunity.getUsers(),
                dao.getCommunityById(1).getUsers());
    }

    @Test
    public void makeUserCommunityAdminTest() {
        dao.makeUserCommunityAdmin(2, 1);
        entityManager.flush();
        List<Account> adminAccounts = dao.getAllUsersByRoleFromCommunity(1, CommunityUserRole.ADMIN);
        assertEquals("CommunityDaoTest.makeUserCommunityAdminTest", 2, adminAccounts.get(1).getId());
    }

    @Test
    public void getIdOfCommunitiesWhereUserIsAdminTest() {
        List<Integer> expectedIdList = new ArrayList<>();
        expectedIdList.add(1);
        assertEquals("AccountDaoTest.getIdOfCommunitiesWhereUserIsAdminTest", expectedIdList,
                dao.getIdOfCommunitiesWhereUserIsAdmin(1));
    }

    @Test
    public void checkUserInsideCommunityTest() {
        assertTrue("AccountDaoTest.checkUserInsideCommunityTest", dao.checkUserInsideCommunity(1, 1));
        assertFalse("AccountDaoTest.checkUserInsideCommunityTest", dao.checkUserInsideCommunity(3, 1));
    }

    @Test
    public void getAllUsersByRoleFromCommunityTest() {
        List<Account> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(getTestAccount(2));
        assertEquals("AccountDaoTest.getAllUsersByRoleFromCommunityTest - user", expectedAccounts,
                dao.getAllUsersByRoleFromCommunity(1, CommunityUserRole.USER));
        expectedAccounts.set(0, getTestAccount(1));
        assertEquals("AccountDaoTest.getAllUsersByRoleFromCommunityTest - admin", expectedAccounts,
                dao.getAllUsersByRoleFromCommunity(1, CommunityUserRole.ADMIN));
    }

    @Test
    public void getCommunityUserByAccountAndCommunityTest() {
        CommunityUser expectedCommunityUser =
                new CommunityUser(getTestAccount(1), getTestCommunity(1), CommunityUserRole.ADMIN);
        assertEquals(
                "AccountDaoTest.getCommunityUserByAccountAndCommunityTest", expectedCommunityUser,
                dao.getCommunityUserByAccountAndCommunity(1, 1));
    }

    @Test
    public void deleteCommunityUser() {
        Community expectedCommunity = getTestCommunity(1);
        List<CommunityUser> communityUsers = new ArrayList<>();
        communityUsers.add(new CommunityUser(getTestAccount(1), expectedCommunity, CommunityUserRole.ADMIN));
        expectedCommunity.setUsers(communityUsers);
        dao.deleteCommunityUser(2, 1);
        entityManager.flush();
        assertEquals(
                "CommunityDaoTest.deleteCommunityUser", expectedCommunity.getUsers(),
                dao.getCommunityById(1).getUsers());
    }

    @Test
    public void updateCommunityNameTest() {
        Community expectedCommunity = getTestCommunity(1);
        expectedCommunity.setCommunityName("New community1");
        dao.updateCommunityName(1, "New community1");
        entityManager.flush();
        assertEquals("CommunityDaoTest.updateCommunityNameTest", expectedCommunity, dao.getCommunityById(1));
    }

    @Test
    public void updateCommunityAvatarByIdTest() {
        byte[] expectedByteAvatar = {18, 18};
        dao.updateAvatarById(1, expectedByteAvatar);
        entityManager.flush();
        assertArrayEquals(
                "CommunityDaoTest.updateCommunityAvatarByIdTest", expectedByteAvatar, dao.getCommunityAvatarById(1));
    }
}
