package com.getjavajob.training.webproject1803.balchevskiyn;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.TestObjectsFactory.getTestAccount;
import static com.getjavajob.training.webproject1803.balchevskiyn.utils.TestObjectsFactory.getTestCommunity;
import static com.getjavajob.training.webproject1803.balchevskiyn.utils.TestObjectsFactory.getTestMessage;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import com.getjavajob.training.webproject1803.balchevskiyn.dao.MessageDao;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.CommunityMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.PersonalMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.RootMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.messages.WallMessage;
import com.getjavajob.training.webproject1803.balchevskiyn.testConfig.TestApplication;
import com.getjavajob.training.webproject1803.balchevskiyn.utils.PrepareDatabaseRule;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TestApplication.class)
@Transactional
public class MessageDaoTest {

    @Rule
    @Autowired
    public PrepareDatabaseRule prepareDatabaseRule;
    @Autowired
    private MessageDao dao;
    @PersistenceContext
    private EntityManager entityManager;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @SuppressWarnings("unchecked")
    @Test
    public void getMessageByIdTest() {
        RootMessage<PersonalMessage> message = getTestMessage("personal");
        assertEquals("MessageDaoTest.getMessageByIdTest", message, dao.getMessageById(1, PersonalMessage.class));
    }

    @Test
    public void getMessageImageByIdTest() {
        byte[] image = {17, 17};
        assertArrayEquals("MessageDaoTest.getMessageImageByIdTest", image,
                dao.getMessageImageById(1, CommunityMessage.class));
    }

    @Test
    public void getMessagesByReceiverAndTypeTest() {
        List<RootMessage> expectedMessages = new ArrayList<>();
        expectedMessages.add(getTestMessage("wall"));
        assertEquals("MessageDaoTest.getMessagesByReceiverAndTypeTest", expectedMessages,
                dao.getMessagesByReceiverAndType(1, WallMessage.class));
    }

    @Test
    public void getAllMessagesBetweenTwoAccountsByTypeTest() {
        List<RootMessage> expectedMessages = new ArrayList<>();
        expectedMessages.add(new PersonalMessage(3, getTestAccount(2), getTestAccount(1),
                "testPersonal", LocalDate.parse("2018-08-03", formatter)));
        expectedMessages.add(getTestMessage("personal"));
        assertEquals("MessageDaoTest.getAllMessagesBetweenTwoAccountsByTypeTest", expectedMessages,
                dao.getAllMessagesBetweenTwoAccountsByType(1, 2, PersonalMessage.class));
    }

    @Test
    public void insertMessageBySenderReceiverAndTypeTest() {
        dao.addMessageBySenderReceiverAndType(2, 3, "test", LocalDate.parse("2018-08-03", formatter),
                PersonalMessage.class);
        List<RootMessage> expectedPersonalMessages = new ArrayList<>();
        expectedPersonalMessages.add(new PersonalMessage(4, getTestAccount(2), getTestAccount(3), "test",
                LocalDate.parse("2018-08-03", formatter)));
        assertEquals("MessageDaoTest.insertMessageBySenderReceiverAndTypeTest", expectedPersonalMessages,
                dao.getAllMessagesBetweenTwoAccountsByType(2, 3, PersonalMessage.class));

        dao.addMessageBySenderReceiverAndType(2, 1, "test", LocalDate.parse("2018-08-03", formatter),
                CommunityMessage.class);
        byte[] image = {17, 17};
        List<RootMessage> expectedCommunityMessages = new ArrayList<>();
        expectedCommunityMessages
                .add(new CommunityMessage(2, getTestAccount(2), getTestCommunity(1), "test", false, null,
                        LocalDate.parse("2018-08-03", formatter)));
        expectedCommunityMessages
                .add(new CommunityMessage(1, getTestAccount(1), getTestCommunity(1), "testCommunity", true, image,
                        LocalDate.parse("2018-08-01", formatter)));
        assertEquals("MessageDaoTest.insertMessageBySenderReceiverAndTypeTest2", expectedCommunityMessages,
                dao.getMessagesByReceiverAndType(1, CommunityMessage.class));

        dao.addMessageBySenderReceiverAndType(1, 2, "test", LocalDate.parse("2018-08-03", formatter),
                WallMessage.class);
        List<RootMessage> expectedWallMessages = new ArrayList<>();
        expectedWallMessages.add(new WallMessage(3, getTestAccount(1), getTestAccount(2), "test", false, null,
                LocalDate.parse("2018-08-03", formatter)));
        assertEquals("MessageDaoTest.insertMessageBySenderReceiverAndTypeTest3", expectedWallMessages,
                dao.getAllMessagesBetweenTwoAccountsByType(1, 2, WallMessage.class));
    }

    @Test
    public void insertMessageImageByIdTest() {
        byte[] image = {18, 18};
        dao.insertMessageImageById(1, image, CommunityMessage.class);
        entityManager.flush();
        assertArrayEquals("MessageDaoTest.insertMessageImageByIdTest", image,
                dao.getMessageImageById(1, CommunityMessage.class));
    }
}
