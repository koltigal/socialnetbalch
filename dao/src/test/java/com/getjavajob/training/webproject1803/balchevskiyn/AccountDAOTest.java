package com.getjavajob.training.webproject1803.balchevskiyn;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.TestObjectsFactory.getTestAccount;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.getjavajob.training.webproject1803.balchevskiyn.Account.AccountRole;
import com.getjavajob.training.webproject1803.balchevskiyn.dao.AccountDao;
import com.getjavajob.training.webproject1803.balchevskiyn.testConfig.TestApplication;
import com.getjavajob.training.webproject1803.balchevskiyn.utils.PrepareDatabaseRule;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TestApplication.class)
@Transactional
public class AccountDaoTest {

    @Rule
    @Autowired
    public PrepareDatabaseRule prepareDatabaseRule;
    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private AccountDao dao;
    @Autowired
    private DataSource dataSource;

    @Test
    public void getAccountsByNameTest() {
        List<Account> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(getTestAccount(1));
        assertEquals("AccountDaoTest.getAccountsByName", expectedAccounts, dao.getAccountsByName("бальч"));
    }

    @Test
    public void getAvatarByIdTest() {
        byte[] expectedByteAvatar = {17, 17};
        assertArrayEquals("AccountDaoTest.getAvatarByIdTest", expectedByteAvatar, dao.getAvatarById(1));
    }

    @Test
    public void getAccountByIdTest() {
        Account expectedAccount = getTestAccount(1);
        assertEquals("AccountDaoTest.getAccountByIdTest", expectedAccount, dao.getAccountById(1));
    }

    @Test
    public void getAccountByEmailTest() {
        Account expectedAccount = getTestAccount(1);
        assertEquals("AccountDaoTest.getAccountByIdTest", expectedAccount, dao.getAccountByEmail("koltigal@gmail.com"));
    }

    @Test
    public void checkAccountsAreFriendsTest() {
        assertTrue("AccountDaoTest.checkAccountsAreFriendsTest", dao.checkAccountsAreFriends(1, 2));
        assertFalse("AccountDaoTest.checkAccountsAreFriendsTest", dao.checkAccountsAreFriends(2, 3));
    }

    @Test
    public void insertAccountTest() {
        Account expectedAcc = getTestAccount(4);
        expectedAcc.setId(null);
        dao.insertAccount(expectedAcc);
        assertEquals("AccountDaoTest.insertAccountTest", expectedAcc, dao.getAccountById(4));
    }

    @Test
    public void deleteAndInsertPhonesTest() {
        Account expectedAcc = getTestAccount(1);
        expectedAcc.setPhones(getTestAccount(2).getPhones());
        dao.deleteAndInsertPhones(1, getTestAccount(2).getPhones());
        assertEquals("AccountDaoTest.deleteAndInsertPhonesTest", expectedAcc, dao.getAccountById(1));
    }

    @Test
    public void insertFriendTest() {
        dao.insertFriend(2, 3);
        entityManager.flush();
        List<Account> expectedFriends = new ArrayList<>();
        expectedFriends.add(getTestAccount(1));

        //I should add additional friend to testAccount because i insert it in DB
        Account testInsertedFriend = getTestAccount(3);
        List<Integer> insertedAccountFriends = new ArrayList<>();
        insertedAccountFriends.add(1);
        insertedAccountFriends.add(2);
        testInsertedFriend.setFriendsIdList(insertedAccountFriends);

        expectedFriends.add(testInsertedFriend);
        assertEquals("AccountDaoTest.insertFriendTest", expectedFriends, dao.getFriendsList(2));
    }

    @Test
    public void deleteFriendTest() {
        dao.deleteFriend(1, 2);
        entityManager.flush();
        assertEquals("AccountDaoTest.deleteFriendTest1", 1, dao.getFriendsList(1).size());
        assertEquals("AccountDaoTest.deleteFriendTest1", 0, dao.getFriendsList(2).size());
    }

    @Test
    public void getFriendsListTest() {
        List<Account> expectedFriends = new ArrayList<>();
        expectedFriends.add(getTestAccount(2));
        expectedFriends.add(getTestAccount(3));
        assertEquals("AccountDaoTest.getFriendsList", expectedFriends, dao.getFriendsList(1));
    }

    @Test
    public void getUsersWithWhichHaveChatTest() {
        List<Account> expectedUsers = new ArrayList<>();
        expectedUsers.add(getTestAccount(2));
        expectedUsers.add(getTestAccount(3));
        assertEquals("AccountDaoTest.getUsersWithWhichHaveChatTest", expectedUsers, dao.getUsersWithWhichHaveChat(1));
    }

    @Test
    public void getPasswordByEmailTest() {
        assertEquals(
                "AccountDaoTest.getPasswordByEmailTest",
                "$2a$12$OhjRaJmjeScNGvxzYk.FoeWTThDFRkkOM/7VnWSQOcgQa19QFepR6",
                dao.getPasswordByEmail("koltigal@gmail.com"));
    }

    @Test
    public void getIdByEmailTest() {
        assertEquals("AccountDaoTest.getIdByEmailTest", 1, (int) dao.getIdByEmail("koltigal@gmail.com"));
    }

    @Test
    public void insertPasswordByIdTest() {
        dao.updatePasswordById(1, "balch");
        assertEquals("AccountDaoTest.insertPasswordByIdTest", "balch", dao.getPasswordByEmail("koltigal@gmail.com"));
    }

    @Test(expected = java.lang.Exception.class)
    public void deleteAccountByIdTest() {
        dao.deleteAccountById(2);
        dao.getAccountById(2);
    }

    @Test
    public void updateNameByIdTest() {
        dao.updateNameById(1, "Тестовый Николай Леонидович");
        assertEquals("AccountDaoTest.updateNameByIdTest", "Тестовый Николай Леонидович",
                dao.getAccountById(1).getName());
    }

    @Test
    public void updateAvatarByIdTest() {
        byte[] testAvatar = {11, 11};
        dao.updateAvatarById(1, testAvatar);
        assertArrayEquals("AccountDaoTest.updateAvatarByIdTest", testAvatar, dao.getAvatarById(1));
    }

    @Test
    public void updateBirthByIdTest() {
        dao.updateBirthById(1, Date.valueOf("2011-01-12"));
        assertEquals("AccountDaoTest.updateBirthByIdTest", Date.valueOf("2011-01-12"),
                dao.getAccountById(1).getBirthDate());
    }

    @Test
    public void updateEmailByIdTest() {
        dao.updateEmailById(1, "test@gmail.com");
        assertEquals("AccountDaoTest.updateEmailByIdTest", "test@gmail.com", dao.getAccountById(1).getEmail());
    }

    @Test
    public void updateIcqByIdTest() {
        dao.updateIcqById(1, 123456789);
        assertEquals("AccountDaoTest.updateIcqByIdTest", 123456789L, (int) dao.getAccountById(1).getIcq());
    }

    @Test
    public void updateSkypeByIdTest() {
        dao.updateSkypeById(1, "testSkype");
        assertEquals("AccountDaoTest.updateSkypeByIdTest", "testSkype", dao.getAccountById(1).getSkype());
    }

    @Test
    public void updateRoleByIdTest() {
        dao.updateRoleById(1, AccountRole.USER);
        entityManager.flush();
        assertEquals("AccountDaoTest.updateRoleByIdTest", AccountRole.USER, dao.getAccountById(1).getRole());
    }
}
