package com.getjavajob.training.webproject1803.balchevskiyn.testConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "com.getjavajob.training.webproject1803.balchevskiyn.dao; com.getjavajob.training.webproject1803.balchevskiyn.utils")
@EnableJpaRepositories("com.getjavajob.training.webproject1803.balchevskiyn.dao")
@EntityScan("com.getjavajob.training.webproject1803.balchevskiyn")
public class TestApplication extends SpringBootServletInitializer {

    private static final Logger logger = LoggerFactory.getLogger(TestApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(TestApplication.class);
    }
}