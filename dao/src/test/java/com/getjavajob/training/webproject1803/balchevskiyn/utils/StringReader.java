package com.getjavajob.training.webproject1803.balchevskiyn.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class StringReader {

    public static String readString(String fileName) {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br =
                new BufferedReader(new InputStreamReader(
                        StringReader.class.getResourceAsStream(fileName),
                        StandardCharsets.UTF_8))) {
            while (br.ready()) {
                sb.append(br.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
