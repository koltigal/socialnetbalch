package com.getjavajob.training.webproject1803.balchevskiyn.utils;

import static com.getjavajob.training.webproject1803.balchevskiyn.utils.StringReader.readString;

import java.sql.Connection;
import javax.sql.DataSource;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PrepareDatabaseRule implements TestRule {

    private DataSource dataSource;

    @Autowired
    public PrepareDatabaseRule(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                try (Connection connection = dataSource.getConnection()) {
                    try (java.sql.Statement statement = connection.createStatement()) {
                        String createTables = readString("SQL create tables scripts.sql");
                        String fillTables = readString("SQL fill tables scripts.sql");
                        statement.executeUpdate(createTables);
                        statement.executeUpdate(fillTables);
                        connection.commit();
                    }

                    base.evaluate();

                    try (java.sql.Statement statement = connection.createStatement()) {
                        String dropTables = readString("SQL drop tables.sql");
                        statement.executeUpdate(dropTables);
                        connection.commit();
                    }
                }
            }
        };
    }
}
