Insert into accounts (full_name, birth, email, icq, skype, password, role, avatar, create_date) values ('Бальчевский Николай Леонидович', '1992-02-12', 'koltigal@gmail.com', 123456789, 'tangoInharlem@skype', '$2a$12$OhjRaJmjeScNGvxzYk.FoeWTThDFRkkOM/7VnWSQOcgQa19QFepR6', 'ADMIN', X'1111', '2018-02-12');
Insert into accounts (full_name, birth, email, icq, skype, password, role, create_date) values ('Почуева Ольга Сергеевна', '1991-02-11', 'Pochuichik@gmail.com', 987654321, 'Pochuichik@skype', '$2a$12$IMdG1N4455HzWbjyiw9WjuYgugqWR.tqOBbNP2eyjBJT8hCkVhnDy', 'USER', '2018-02-12');
Insert into accounts (full_name, birth, email, icq, skype, password, role, create_date) values ('Пупкин Василий Васильевич', '2000-11-21', 'Vasys@mail.com', 444444444, 'Vasil@skype', '$2a$12$KZwadESnNaMh9xfUjvLaFumRbpE3jPqVjzH3vDpa7uJRgJhfMdTmK', 'USER', '2018-02-12');

Insert into friend_requests (requesting_user_id, requested_user_id) values (2, 3);
Insert into friend_requests (requesting_user_id, requested_user_id) values (3, 1);

Insert into friends (user_id, user_friend_id) values (1, 2);
Insert into friends (user_id, user_friend_id) values (1, 3);
Insert into friends (user_id, user_friend_id) values (2, 1);
Insert into friends (user_id, user_friend_id) values (3, 1);

Insert into phones (user_id, phone) values (1, '+79037747207');
Insert into phones (user_id, phone) values (1, '+79037747208');
Insert into phones (user_id, phone) values (2, '+79022222222');
Insert into phones (user_id, phone) values (2, '+79033333333');
Insert into phones (user_id, phone) values (3, '+79000000000');
Insert into phones (user_id, phone) values (3, '+79111111111');

Insert into communities (community_name, community_avatar, create_date, description, creator_id) values ('New community', X'1111', '2018-08-01', 'Новая группа', 1);
Insert into communities (community_name, create_date, description, creator_id) values ('New community2', '2018-08-01', 'Новая группа2', 3);

Insert into community_users (community_id, user_id, user_role) values (1, 1, 'ADMIN');
Insert into community_users (community_id, user_id, user_role) values (1, 2, 'USER');
Insert into community_users (community_id, user_id, user_role) values (2, 3, 'ADMIN');

Insert into community_requests (requesting_user_id, requested_community_id, message) values (3, 1, 'test');
Insert into community_requests (requesting_user_id, requested_community_id, message) values (1, 2, 'test');

Insert into personal_messages (sender, receiver, text, date) values (1, 2, 'testPersonal', '2018-08-01');
Insert into personal_messages (sender, receiver, text, date) values (1, 3, 'testPersonal', '2018-08-02');
Insert into personal_messages (sender, receiver, text, date) values (2, 1, 'testPersonal', '2018-08-03');

Insert into community_messages (sender, receiver, text, photo, date) values (1, 1, 'testCommunity', X'1111', '2018-08-01');

Insert into wall_messages (sender, receiver, text, date) values (3, 1, 'testWall', '2018-08-01');
Insert into wall_messages (sender, receiver, text, date) values (3, 2, 'testWall', '2018-08-01');


