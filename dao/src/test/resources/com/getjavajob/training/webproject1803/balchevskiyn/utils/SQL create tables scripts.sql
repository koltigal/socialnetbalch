CREATE TABLE `accounts` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `birth` date DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `icq` varchar(20) DEFAULT NULL,
  `skype` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `avatar` MEDIUMBLOB DEFAULT NULL,
  `create_date` DATE NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `friends` (
  `user_id` int(5) NOT NULL,
  `user_friend_id` int(5) NOT NULL,
  PRIMARY KEY (`user_id`,`user_friend_id`),
  KEY `friends_ibfk_1` (`user_id`),
  KEY `friends_ibfk_2` (`user_friend_id`),
  CONSTRAINT `friends_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `friends_ibfk_2` FOREIGN KEY (`user_friend_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE
);

CREATE TABLE `friend_requests` (
  `requesting_user_id` int(5) NOT NULL,
  `requested_user_id` int(5) NOT NULL,
  PRIMARY KEY (`requesting_user_id`,`requested_user_id`),
  CONSTRAINT `friendRequest_ibfk_1` FOREIGN KEY (`requesting_user_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `friendRequest_ibfk_2` FOREIGN KEY (`requested_user_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE
);

CREATE TABLE `personal_messages` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `sender` int(5) NOT NULL,
  `receiver` int(5) NOT NULL,
  `text` varchar(255) NOT NULL,
  `photo` MEDIUMBLOB DEFAULT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `personal_messages_ibfk_1` FOREIGN KEY (`sender`) REFERENCES `accounts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `personal_messages_ibfk_2` FOREIGN KEY (`receiver`) REFERENCES `accounts` (`id`) ON DELETE CASCADE
);

CREATE TABLE `wall_messages` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `sender` int(5) NOT NULL,
  `receiver` int(5) NOT NULL,
  `text` varchar(255) NOT NULL,
  `photo` MEDIUMBLOB DEFAULT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `wall_messages_ibfk_1` FOREIGN KEY (`sender`) REFERENCES `accounts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wall_messages_ibfk_2` FOREIGN KEY (`receiver`) REFERENCES `accounts` (`id`) ON DELETE CASCADE
);

CREATE TABLE `communities` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `community_name` varchar(255) NOT NULL,
  `community_avatar` MEDIUMBLOB DEFAULT NULL,
  `create_date` DATE NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `creator_id` int(5) NOT NULL,
   PRIMARY KEY (`id`),
  CONSTRAINT `communities_ibfk_1` FOREIGN KEY (`creator_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE
);

CREATE TABLE `community_messages` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `sender` int(5) NOT NULL,
  `receiver` int(5) NOT NULL,
  `text` varchar(255) NOT NULL,
  `photo` MEDIUMBLOB DEFAULT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `community_messages_ibfk_1` FOREIGN KEY (`sender`) REFERENCES `accounts` (`id`) ON DELETE CASCADE
  ,CONSTRAINT `community_messages_ibfk_2` FOREIGN KEY (`receiver`) REFERENCES `communities` (`id`) ON DELETE CASCADE
);

CREATE TABLE `community_requests` (
  `requesting_user_id` int(5) NOT NULL,
  `requested_community_id` int(5) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`requesting_user_id`,`requested_community_id`),
  CONSTRAINT `communityRequest_ibfk_1` FOREIGN KEY (`requesting_user_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `communityRequest_ibfk_2` FOREIGN KEY (`requested_community_id`) REFERENCES `communities` (`id`) ON DELETE CASCADE
);

CREATE TABLE `community_users` (
  `community_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `user_role` varchar(255) NOT NULL,
  PRIMARY KEY (`community_id`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `community_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `community_users_ibfk_2` FOREIGN KEY (`community_id`) REFERENCES `communities` (`id`) ON DELETE CASCADE
);

CREATE TABLE `phones` (
  `user_id` int(5) NOT NULL,
  `phone` varchar(16) DEFAULT NULL,
  UNIQUE KEY (`user_id`,`phone`),
  KEY `phones_ibfk_1` (`user_id`),
  CONSTRAINT `phones_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE
);